package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.PublishMessage;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PublishMessageReaderTest {

  @Test
  public void simple() throws IOException {

    final var fname = List.of("toto.txt", "tata.txt");
    final var test1 = new PublishMessage(fname);
    var test = test1.toByteBuffer().flip();
    test.getInt(); // retrieve the id of the trame.
    test.compact();
    final var reader = new PublishMessageReader();
    System.out.println(test1);
    assertEquals(Reader.ProcessStatus.DONE, reader.process(test));
    assertEquals(test1, reader.get());
    assertEquals(0, test.position());
    assertEquals(test.capacity(), test.limit());
  }
}
