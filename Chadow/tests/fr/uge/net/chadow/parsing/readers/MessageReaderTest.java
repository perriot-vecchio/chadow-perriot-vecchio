package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.readers.MessageReader;
import fr.uge.net.chadow.parsing.readers.Reader;
import fr.uge.net.chadow.parsing.messages.PublicMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageReaderTest {

  //create test like StringReaderTest

  @Test
  public void simple() {
    var login = "login";
    var message = "message";
    var bb = ByteBuffer.allocate(1024);
    var bytesLogin = StandardCharsets.UTF_8.encode(login);
    var bytesMessage = StandardCharsets.UTF_8.encode(message);
    bb.putInt(bytesLogin.remaining()).put(bytesLogin).putInt(bytesMessage.remaining()).put(bytesMessage);
    MessageReader mr = new MessageReader();
    Assertions.assertEquals(Reader.ProcessStatus.DONE, mr.process(bb));
    assertEquals(new PublicMessage(login, message), mr.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }

  @Test
  public void reset() {
    var login = "login";
    var message = "message";
    var login2 = "login2";
    var message2 = "message2";
    var bb = ByteBuffer.allocate(1024);
    var bytesLogin = StandardCharsets.UTF_8.encode(login);
    var bytesMessage = StandardCharsets.UTF_8.encode(message);
    var bytesLogin2 = StandardCharsets.UTF_8.encode(login2);
    var bytesMessage2 = StandardCharsets.UTF_8.encode(message2);
    bb.putInt(bytesLogin.remaining()).put(bytesLogin).putInt(bytesMessage.remaining()).put(bytesMessage).putInt(bytesLogin2.remaining()).put(bytesLogin2).putInt(bytesMessage2.remaining()).put(bytesMessage2);
    MessageReader mr = new MessageReader();
    assertEquals(Reader.ProcessStatus.DONE, mr.process(bb));
    assertEquals(new PublicMessage(login, message), mr.get());
    assertEquals(22, bb.position());
    assertEquals(bb.capacity(), bb.limit());
    mr.reset();
    assertEquals(Reader.ProcessStatus.DONE, mr.process(bb));
    assertEquals(new PublicMessage(login2, message2), mr.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }

  @Test
  public void smallBuffer() {
    var login = "login";
    var message = "message";
    var bb = ByteBuffer.allocate(1024);
    var bytesLogin = StandardCharsets.UTF_8.encode(login);
    var bytesMessage = StandardCharsets.UTF_8.encode(message);
    bb.putInt(bytesLogin.remaining()).put(bytesLogin).putInt(bytesMessage.remaining()).put(bytesMessage).flip();
    var bbSmall = ByteBuffer.allocate(2);
    var mr = new MessageReader();
    while (bb.hasRemaining()) {
      while (bb.hasRemaining() && bbSmall.hasRemaining()) {
        bbSmall.put(bb.get());
      }
      if (bb.hasRemaining()) {
        assertEquals(Reader.ProcessStatus.REFILL, mr.process(bbSmall));
      } else {
        assertEquals(Reader.ProcessStatus.DONE, mr.process(bbSmall));
        assertEquals(new PublicMessage(login, message), mr.get());
      }
    }
  }

}
