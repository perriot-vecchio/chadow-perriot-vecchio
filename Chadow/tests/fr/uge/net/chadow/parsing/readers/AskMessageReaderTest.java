package fr.uge.net.chadow.parsing.readers;

import org.junit.jupiter.api.Test;

import fr.uge.net.chadow.parsing.messages.AskMessage;

import java.nio.ByteBuffer;

import static fr.uge.net.chadow.Configuration.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AskMessageReaderTest {

  @Test
  public void simple() {
    byte mode = 0;
    String fichier = "totogggh";
    var bb = ByteBuffer.allocate(1024);
    var bytesFichier = UTF_8.encode(fichier);
    bb.put(mode).putInt(bytesFichier.remaining()).put(bytesFichier);

    AskMessageReader ar = new AskMessageReader();
    assertEquals(Reader.ProcessStatus.DONE, ar.process(bb));
    assertEquals(new AskMessage(mode, fichier), ar.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }
}
