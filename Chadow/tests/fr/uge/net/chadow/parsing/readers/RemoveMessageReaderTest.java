package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.RemoveMessage;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.util.List;

import static fr.uge.net.chadow.Configuration.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RemoveMessageReaderTest {

  @Test
  public void simple() {
    int nbFiles = 3;
    String fName1 = "file1";
    String fName2 = "file2";
    String fName3 = "file3";

    var bb = ByteBuffer.allocate(1024);

    var bytesFName1 = UTF_8.encode(fName1);
    var bytesFName2 = UTF_8.encode(fName2);
    var bytesFName3 = UTF_8.encode(fName3);

    bb.putInt(nbFiles)
      .putInt(bytesFName1.remaining()).put(bytesFName1)
      .putInt(bytesFName2.remaining()).put(bytesFName2)
      .putInt(bytesFName3.remaining()).put(bytesFName3);

    RemoveMessageReader ar = new RemoveMessageReader();
    assertEquals(Reader.ProcessStatus.DONE, ar.process(bb));
    assertEquals(new RemoveMessage(List.of(fName1, fName2, fName3)), ar.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }

}
