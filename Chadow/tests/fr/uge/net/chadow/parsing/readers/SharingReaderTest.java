package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.PublicMessage;
import fr.uge.net.chadow.parsing.messages.SharingMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SharingReaderTest {

  @Test
  public void SharingMessageOpenMode() {
    final var msg = new SharingMessage("toto.txt", 99, "toto");
    final var bb = msg.toByteBuffer();
    final var reader = new SharingReader();

    bb.flip();
    bb.getInt();
    bb.compact();


    assertEquals(Reader.ProcessStatus.DONE, reader.process(bb));
    assertEquals(msg, reader.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }

  @Test
  public void SharingMessageHiddenMode() {
    final var msg = new SharingMessage("toto.txt", 99);
    final var bb = msg.toByteBuffer();
    final var reader = new SharingReader();

    bb.flip();
    bb.getInt();
    bb.compact();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(bb));
    assertEquals(msg, reader.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }
}
