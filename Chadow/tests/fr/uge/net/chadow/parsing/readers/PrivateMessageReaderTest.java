package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.readers.PrivateMessageReader;
import fr.uge.net.chadow.parsing.readers.Reader;
import fr.uge.net.chadow.parsing.messages.PrivateMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrivateMessageReaderTest {

  @Test
  public void simple() {
    var sender = "sender";
    var receiver = "receiver";
    var message = "message a envoyé";
    var bb = ByteBuffer.allocate(1024);
    var bytesSender = StandardCharsets.UTF_8.encode(sender);
    var bytesReceiver = StandardCharsets.UTF_8.encode(receiver);
    var bytesMessage = StandardCharsets.UTF_8.encode(message);
    bb.putInt(bytesSender.remaining()).put(bytesSender).putInt(bytesReceiver.remaining()).put(bytesReceiver).putInt(bytesMessage.remaining()).put(bytesMessage);
    PrivateMessageReader ar = new PrivateMessageReader();
    Assertions.assertEquals(Reader.ProcessStatus.DONE, ar.process(bb));
    assertEquals(new PrivateMessage(sender, receiver, message), ar.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }
}
