package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.FileListMessage;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileListReaderTest {

  @Test
  public void simple() {
    List<String> files = List.of("file1", "file2", "file3");
    FileListMessage message = new FileListMessage(files);
    ByteBuffer buffer = message.toByteBuffer().flip();
    buffer.getInt(); // retrieve the id of the trame.

//    System.out.println("nbFiles : " + nbFiles);
//    var fname_size = buffer.getInt();
//    System.out.println("fname_size : " + fname_size);
//    System.out.println("files : " + UTF_8.decode(buffer.limit(buffer.position() + fname_size)));
    buffer.compact();

    FileListReader reader = new FileListReader();
    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals(message, reader.get());
    assertEquals(0, buffer.position());
    assertEquals(buffer.capacity(), buffer.limit());

  }
}
