package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.readers.AuthenticationReader;
import fr.uge.net.chadow.parsing.readers.Reader;
import fr.uge.net.chadow.parsing.messages.Authentication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static fr.uge.net.chadow.Configuration.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthenticationReaderTest {

  @Test
  public void simple() {

    var listener = 2;
    var login = "login";
    var bb = ByteBuffer.allocate(1024);
    var bytesLogin = UTF_8.encode(login);
    bb.putInt(listener).putInt(bytesLogin.remaining()).put(bytesLogin);
    AuthenticationReader ar = new AuthenticationReader();
    Assertions.assertEquals(Reader.ProcessStatus.DONE, ar.process(bb));
    assertEquals(new Authentication(listener, login), ar.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }
}
