package fr.uge.net.chadow.parsing.readers;

import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ByteReaderTest {

  @Test
  public void simple() {
    final var reader = new ByteReader();
    final var bb = ByteBuffer.allocate(1);

    bb.put((byte) 1);

    assertEquals(Reader.ProcessStatus.DONE, reader.process(bb));
    assertEquals((byte) 1, reader.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }
}
