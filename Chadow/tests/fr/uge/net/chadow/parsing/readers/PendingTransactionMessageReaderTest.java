package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.PendingTransactionMessage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PendingTransactionMessageReaderTest {

  @Test
  public void simple() {
    var pt = new PendingTransactionMessage((byte) 1, 42);
    var reader = new PendingTransactionMessageReader();
    var bb = pt.toByteBuffer();
    bb.flip();
    bb.getInt();
    bb.compact();
    assertEquals(Reader.ProcessStatus.DONE, reader.process(bb));
    assertEquals(pt, reader.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }
}
