package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.TransactionMessage;
import org.junit.jupiter.api.Test;

import java.net.InetSocketAddress;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionMessageReaderTest {

  @Test
  void simple() {
    var transactionMessage = new TransactionMessage(10000, "toto", List.of(
        new InetSocketAddress("ipv6", 8080),
        new InetSocketAddress("ipv4", 8080),
        new InetSocketAddress("ipv4", 8080)
    ));
    var reader = new TransactionMessageReader();
    var bb = transactionMessage.toByteBuffer();

    bb.flip();
    bb.getInt(); // retrieve the id
    bb.compact();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(bb));
    assertEquals(transactionMessage, reader.get());
    assertEquals(0, bb.position());
    assertEquals(bb.capacity(), bb.limit());
  }
}
