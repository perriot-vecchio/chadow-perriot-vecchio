Tu viens d'incendier la Bibliothèque ?

- Oui.
J'ai mis le feu là.

- Mais c'est un crime inouï !
Crime commis par toi contre toi-même, infâme !
Mais tu viens de tuer le rayon de ton âme !
C'est ton propre flambeau que tu viens de souffler !
Ce que ta rage impie et folle ose brûler,
C'est ton bien, ton trésor, ta dot, ton héritage
Le livre, hostile au maître, est à ton avantage.
Le livre a toujours pris fait et cause pour toi.
Une bibliothèque est un acte de foi
Des générations ténébreuses encore
Qui rendent dans la nuit témoignage à l'aurore.
Quoi! dans ce vénérable amas des vérités,
Dans ces chefs-d'oeuvre pleins de foudre et de clartés,
Dans ce tombeau des temps devenu répertoire,
Dans les siècles, dans l'homme antique, dans l'histoire,
Dans le passé, leçon qu'épelle l'avenir,
Dans ce qui commença pour ne jamais finir,
Dans les poètes! quoi, dans ce gouffre des bibles,
Dans le divin monceau des Eschyles terribles,
Des Homères, des jobs, debout sur l'horizon,
Dans Molière, Voltaire et Kant, dans la raison,
Tu jettes, misérable, une torche enflammée !
De tout l'esprit humain tu fais de la fumée !
As-tu donc oublié que ton libérateur,
C'est le livre ? Le livre est là sur la hauteur;
Il luit; parce qu'il brille et qu'il les illumine,
Il détruit l'échafaud, la guerre, la famine
Il parle, plus d'esclave et plus de paria.
Ouvre un livre. Platon, Milton, Beccaria.
Lis ces prophètes, Dante, ou Shakespeare, ou Corneille
L'âme immense qu'ils ont en eux, en toi s'éveille ;
Ébloui, tu te sens le même homme qu'eux tous ;
Tu deviens en lisant grave, pensif et doux ;
Tu sens dans ton esprit tous ces grands hommes croître,
Ils t'enseignent ainsi que l'aube éclaire un cloître
À mesure qu'il plonge en ton coeur plus avant,
Leur chaud rayon t'apaise et te fait plus vivant ;
Ton âme interrogée est prête à leur répondre ;
Tu te reconnais bon, puis meilleur; tu sens fondre,
Comme la neige au feu, ton orgueil, tes fureurs,
Le mal, les préjugés, les rois, les empereurs !
Car la science en l'homme arrive la première.
Puis vient la liberté. Toute cette lumière,
C'est à toi comprends donc, et c'est toi qui l'éteins !
Les buts rêvés par toi sont par le livre atteints.
Le livre en ta pensée entre, il défait en elle
Les liens que l'erreur à la vérité mêle,
Car toute conscience est un noeud gordien.
Il est ton médecin, ton guide, ton gardien.
Ta haine, il la guérit ; ta démence, il te l'ôte.
Voilà ce que tu perds, hélas, et par ta faute !
Le livre est ta richesse à toi ! c'est le savoir,
Le droit, la vérité, la vertu, le devoir,
Le progrès, la raison dissipant tout délire.
Et tu détruis cela, toi !

- Je ne sais pas lire.