package fr.uge.net.chadow.client;

import fr.uge.net.chadow.contexts.Context;

import java.util.HashMap;
import java.util.Map;

public class DownloadTracker {
  private State state = State.WAITING;

  boolean launch(Context... context) {
    return state != State.PENDING;
  }

  Map<Context, Integer> downloadsRepartition = new HashMap<>();
  int fSize = 0;
  int cmp = 0;

  public int next(Context c) {
    final var current = cmp++;
    return downloadsRepartition.merge(c, current, (old, n) -> current);
  }

  private enum State {
    WAITING, PENDING
  }

}
