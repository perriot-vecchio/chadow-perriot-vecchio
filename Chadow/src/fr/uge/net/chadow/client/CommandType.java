package fr.uge.net.chadow.client;

public enum CommandType {
  PUBLIC_MESSAGE(""),
  PRIVATE_MESSAGE("tell"),
  PUBLISH_FILE("publish"),
  REMOVE_FILE("remove"),
  LIST_FILE("list"),
  CLOSE("exit"),
  DOWNLOAD_FILE("ask"),
  ACCEPT("accept"),
  REJECT("reject"),
  HELP("help");
  private final String prefix;

  CommandType(String prefix) {
    this.prefix = prefix;
  }

  public String prefix() {
    return prefix;
  }

  static String usage() {
    return """
        CHADOW :
        - Write something to send a message to everyone.
        - "/tell [pseudo]" to send a private message to pseudo.
        - "/publish [file1] [file2] ..." to publish files to the server.
        - "/remove [file1] [file2] ..." to stop publishing those files to the server.
        - "/list" to get a list of all files published by the server.
        - "/exit" to stop the program.
        - "/ask [file]" to start downloading procedure for this file.
        - "/accept [id]" to accept sharing file of transactionId=id .
        - "/reject [id]" to refuse sharing file of transactionId=id .
        - "/help" to get this message.
        """;
  }
}
