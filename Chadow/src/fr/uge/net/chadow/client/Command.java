package fr.uge.net.chadow.client;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import static fr.uge.net.chadow.client.CommandType.PUBLIC_MESSAGE;

public record Command(CommandType type, String args) {
  public Command {
    Objects.requireNonNull(type);
    Objects.requireNonNull(args);
  }

  public static Command parseCommand(String msg) throws IOException {
    var words = msg.split(" ");
    if (msg.startsWith("/")) {
      var prefix = words[0].replaceFirst("/", "");
      var args = String.join(" ", Arrays.stream(words).skip(1).toList());
      var commandType = Arrays.stream(CommandType.values())
        .filter(type -> prefix.equals(type.prefix()))
        .findAny()
        .orElseThrow(IOException::new);
      return new Command(commandType, args);
    }
    return new Command(PUBLIC_MESSAGE, String.join(" ", words));
  }

  @Override
  public String toString() {
    if (type == PUBLIC_MESSAGE) {
      return args;
    }
    return "/" + type.prefix() + " " + args;
  }
}
