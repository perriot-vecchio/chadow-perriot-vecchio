package fr.uge.net.chadow.client;

import fr.uge.net.chadow.contexts.ClientContext;
import fr.uge.net.chadow.contexts.Context;
import fr.uge.net.chadow.contexts.UploadContext;
import fr.uge.net.chadow.parsing.messages.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;
import java.util.logging.Logger;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;


public class Client {
  private static final Logger logger = Logger.getLogger(Client.class.getName());
  private final SocketChannel sc;
  private final Selector selector;
  private final InetSocketAddress serverAddress;
  private final String login;
  private final Thread console;
  private final Deque<Command> commandQueue = new ArrayDeque<>();
  private final int listenerPort;
  private final ServerSocketChannel uploadListener;

  public Client(String login, InetSocketAddress serverAddress, int listenerPort) throws IOException {
    this.serverAddress = serverAddress;
    this.login = login;
    this.sc = SocketChannel.open();
    this.selector = Selector.open();
    this.console = Thread.ofPlatform().daemon().unstarted(this::consoleRun);
    this.listenerPort = listenerPort;

    uploadListener = ServerSocketChannel.open();
    uploadListener.configureBlocking(false);
    uploadListener.bind(new InetSocketAddress(listenerPort));
  }

  private void consoleRun() {
    try (var scanner = new Scanner(System.in)) {
      while (scanner.hasNextLine()) {
        var msg = scanner.nextLine();
        sendCommand(msg);
      }
    }
    logger.info("Console thread stopping");
  }

  /**
   * Send instructions to the selector via a BlockingQueue and wake it up
   */
  private void sendCommand(String msg) {
    try {
      if (msg.length() > BUFFER_SIZE) {
        logger.warning("message exceed max length");
        return;
      }
      if (!msg.isBlank() && commandQueue.offer(Command.parseCommand(msg))) {
        selector.wakeup();
      }
    } catch (IOException e) {
      logger.info("Command unknown");
    }
  }

  public static void main(String[] args) throws NumberFormatException, IOException {
    if (args.length != 4) {
      usage();
      return;
    }
    new Client(args[0], new InetSocketAddress(args[1], Integer.parseInt(args[2])), Integer.parseInt(args[3])).launch();
  }

  private void treatKey(SelectionKey key) {
    try {
      if (key.isValid() && key.isAcceptable()) {
        doAccept(key);
      }
    } catch (IOException ioe) {
      // lambda call in select requires to tunnel IOException
      throw new UncheckedIOException(ioe);
    }
    try {
      final var context = (Context) key.attachment();
      if (key.isValid() && key.isConnectable()) {
        context.doConnect();
      }
      if (key.isValid() && key.isWritable()) {
        context.doWrite();
      }
      if (key.isValid() && key.isReadable()) {
        context.doRead();
      }
    } catch (IOException ioe) {
      // lambda call in select requires to tunnel IOException
      throw new UncheckedIOException(ioe);
    }
  }

  private static void usage() {
    System.out.println("Usage : ClientChat login hostname portServer listenerPort");
  }

  /**
   * Processes the command from the BlockingQueue
   */
  private void processCommands(ClientContext context) {
    var command = commandQueue.poll();
    if (command == null) {
      return;
    }
    switch (command.type()) {
      case PUBLIC_MESSAGE -> {
        final var message = new PublicMessage(login, command.args());
        context.queueMessage(message);
      }
      case PRIVATE_MESSAGE -> {
        final var args = command.args().split(" ");
        if (args.length < 2) {
          logger.warning("Not enough arguments " + args.length);
          return;
        }
        final var receiver = args[0];
        final var message = String.join(" ", Arrays.stream(args).skip(1).toList());
        context.queueMessage(new PrivateMessage(login, receiver, message));
      }
      case PUBLISH_FILE -> {
        final var args = command.args().split(" ");
        if (args.length < 1) {
          logger.warning("Not enough arguments");
          return;
        }
        final var listFiles = Arrays.stream(args).toList();
        try {
          context.queueMessage(new PublishMessage(listFiles));
          System.out.println("Message sent !");
        } catch (IOException ioException) {
          throw new UncheckedIOException(ioException);
        }
      }
      case REMOVE_FILE -> {
        final var args = command.args().split(" ");
        if (args.length < 1) {
          logger.warning("Not enough arguments");
          return;
        }
        final var listFiles = Arrays.stream(args).toList();
        context.queueMessage(new RemoveMessage(listFiles));
        System.out.println("Message sent !");
      }
      case LIST_FILE -> context.queueMessage(new CommandListMessage());
      case CLOSE -> {
        context.silentlyClose();
        console.interrupt();
        Thread.currentThread().interrupt();
      }
      case DOWNLOAD_FILE -> {
        final var args = command.args().split(" ");
        if (args.length < 1) {
          logger.warning("Not enough arguments");
          return;
        }
        if (args.length == 1) {
          context.queueMessage(new AskMessage((byte) 0, args[0]));
        } else if (args.length == 2) {
          context.queueMessage(new AskMessage((byte) Integer.parseInt(args[0]), args[1]));
        }
        System.out.println("Message sent !");
      }
      case ACCEPT, REJECT -> {
        final var args = command.args().split(" ");
        if (args.length < 1) {
          logger.warning("Not enough arguments");
          return;
        }
        context.queueMessage(command.type() == CommandType.REJECT
            ? new PendingTransactionMessage((byte) 0, Integer.parseInt(args[0]))
            : new PendingTransactionMessage((byte) 1, Integer.parseInt(args[0]))
        );
        System.out.println("Message sent !");
      }
      case HELP -> System.out.println(CommandType.usage());
    }
  }

  public void launch() throws IOException {
    sc.configureBlocking(false);
    final var clientKey = sc.register(selector, SelectionKey.OP_CONNECT);
    final var context = new ClientContext(selector, clientKey, listenerPort, login);

    clientKey.attach(context);
    sc.connect(serverAddress);
    uploadListener.register(selector, SelectionKey.OP_ACCEPT);

    console.start();

    while (!Thread.interrupted()) {
      try {
        selector.select(this::treatKey);
        processCommands(context);
      } catch (UncheckedIOException tunneled) {
        throw tunneled.getCause();
      }
    }
  }

  private void doAccept(SelectionKey key) throws IOException {
    ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
    SocketChannel sc = ssc.accept();
    if (sc == null) {
      return;
    }
    sc.configureBlocking(false);
    final var uploadKey = sc.register(selector, SelectionKey.OP_READ);
    uploadKey.interestOps(SelectionKey.OP_READ);
    uploadKey.attach(new UploadContext(uploadKey));

  }
}
