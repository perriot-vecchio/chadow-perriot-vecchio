package fr.uge.net.chadow;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public final class Configuration {
  public static final int BUFFER_SIZE = 1024;
  public static final Charset UTF_8 = StandardCharsets.UTF_8;
}
