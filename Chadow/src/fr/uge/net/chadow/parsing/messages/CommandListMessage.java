package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.nio.ByteBuffer;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;

/**
 * Follow a "/list". Do nothing server's side.
 */
public class CommandListMessage implements Trame {
  @Override
  public ByteBuffer toByteBuffer() {
    return ByteBuffer.allocate(BUFFER_SIZE).putInt(ClientServerType.LIST.id());
  }
}
