package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.nio.ByteBuffer;
import java.util.Objects;

import static fr.uge.net.chadow.Configuration.UTF_8;

public record PublicMessage(String login, String message) implements Trame {
  public PublicMessage {
    Objects.requireNonNull(login);
    Objects.requireNonNull(message);
  }

  @Override
  public ByteBuffer toByteBuffer() {
    var bytesLogin = UTF_8.encode(login);
    var bytesMessage = UTF_8.encode(message);
    return ByteBuffer.allocate(1024)
      .putInt(ClientServerType.ALL.id())
      .putInt(bytesLogin.remaining())
      .put(bytesLogin)
      .putInt(bytesMessage.remaining())
      .put(bytesMessage);
  }

  @Override
  public String toString() {
    return login + ": " + message;
  }
}
