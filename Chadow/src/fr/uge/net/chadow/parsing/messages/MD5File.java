package fr.uge.net.chadow.parsing.messages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Objects;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public class MD5File {
  private final String name;
  private final ByteBuffer md5;
  private int size;

  public MD5File(String name, ByteBuffer md5) throws IOException {
    Objects.requireNonNull(name);
    Objects.requireNonNull(md5);
    //try to get the size of the file named name
    this.name = name;
    this.md5 = md5;
    try (FileChannel fc = FileChannel.open(Path.of(name), StandardOpenOption.READ)) {
      size = (int) fc.size(); //assuming the file is less than 2Gb bytes
    } catch (NoSuchFileException | FileNotFoundException e) {
      throw new IOException(e);
    }
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    md5.flip();
    for (; md5.hasRemaining(); sb.append(String.format("%02x", md5.get()))) ;
    return "MD5File{" + "name='" + name + '\'' + ", md5=" + sb + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MD5File md5File = (MD5File) o;
    return Objects.equals(name, md5File.name) && Arrays.equals(md5.array(), md5File.md5.array());
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, md5);
  }

  public String name() {
    return name;
  }

  public ByteBuffer md5() {
    return md5;
  }

  public int size() {
    return size;
  }
}