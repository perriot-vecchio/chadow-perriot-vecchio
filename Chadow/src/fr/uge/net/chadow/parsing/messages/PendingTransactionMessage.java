package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.nio.ByteBuffer;

public record PendingTransactionMessage(byte accept, int id) implements Trame {

  @Override
  public ByteBuffer toByteBuffer() {
    ByteBuffer buff = ByteBuffer.allocate(Integer.BYTES * 2 + Byte.BYTES);
    return buff.putInt(ClientServerType.PENDING_TRANSACTION.id()).put(accept).putInt(id);
  }

  public boolean hasAccepted() {
    return accept == (byte) 1;
  }

}
