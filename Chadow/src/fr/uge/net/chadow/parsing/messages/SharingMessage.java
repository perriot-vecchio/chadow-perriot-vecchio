package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ServerClientType;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.Optional;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public record SharingMessage(String fileName, int transactionId, byte mode, String pseudo) implements Trame {
  private static final String WITHOUT_PSEUDO = "Would you like to share %s ? Pending transaction id: %d";
  private static final String WITH_PSEUDO = "Would you like to share %s with %s ? Pending transaction id: %d";

  public SharingMessage {
    Objects.requireNonNull(fileName);
  }

  public SharingMessage(String fileName, int transactionId, String pseudo) {
    this(fileName, transactionId, (byte) 0, pseudo);
  }

  public SharingMessage(String fileName, int transactionId) {
    this(fileName, transactionId, (byte) 1, null);
  }


  @Override
  public ByteBuffer toByteBuffer() {
    final var buffer = ByteBuffer.allocate(BUFFER_SIZE);
    final var sendFileName = UTF_8.encode(fileName);
    buffer.putInt(ServerClientType.SHARING.id()).putInt(transactionId);
    buffer.putInt(sendFileName.remaining()).put(sendFileName);
    buffer.put(mode);
    Optional.ofNullable(pseudo).ifPresent(pseudo -> {
      final var sendPseudo = UTF_8.encode(pseudo);
      final var sizePseudo = sendPseudo.remaining();
      buffer.putInt(sizePseudo).put(sendPseudo);
    });
    return buffer;
  }

  @Override
  public String toString() {
    return (pseudo == null
        ? String.format(WITHOUT_PSEUDO, fileName, transactionId)
        : String.format(WITH_PSEUDO, fileName, pseudo, transactionId))
        + "\n /accept [id] or /reject [id] to answer !";
  }
}
