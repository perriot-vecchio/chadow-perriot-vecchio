package fr.uge.net.chadow.parsing.messages;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;
import static fr.uge.net.chadow.parsing.types.ServerClientType.TRANSACTION;

public record TransactionMessage(int fileSize, String fileName, List<InetSocketAddress> providers) implements Trame {

  public TransactionMessage {
    Objects.requireNonNull(fileName);
    Objects.requireNonNull(providers);
  }

  public void forEachProvider(Consumer<InetSocketAddress> consumer) {
    providers.forEach(consumer::accept);
  }

  @Override
  public ByteBuffer toByteBuffer() {
    ByteBuffer buff = ByteBuffer.allocate(BUFFER_SIZE);
    final var utf8Name = UTF_8.encode(fileName);
    final var fileNameSize = utf8Name.remaining();

    buff.putInt(TRANSACTION.id())
            .putInt(fileSize)
            .putInt(fileNameSize)
            .put(utf8Name)
            .putInt(providers.size());

    for (InetSocketAddress provider : providers) {
      var bytesAddress = UTF_8.encode(provider.getHostString());
      buff.putInt(bytesAddress.remaining()).put(bytesAddress).putInt(provider.getPort());
    }
    return buff;
  }
}
