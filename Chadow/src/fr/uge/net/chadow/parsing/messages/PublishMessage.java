package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.logging.Logger;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public class PublishMessage implements Trame {
  private static final Logger logger = Logger.getLogger(PublishMessage.class.getName());
  private static final int MD5_SIZE = 16;
  private static MessageDigest digest;
  private final List<MD5File> files = new ArrayList<>();
  private List<Integer> sizes = new ArrayList<>();

  public PublishMessage(List<String> names, List<ByteBuffer> md5, List<Integer> sizes) throws IOException {
    if (names.size() != md5.size()) {
      throw new IllegalArgumentException();
    }
    final var nameIter = names.iterator();
    final var md5Iter = md5.iterator();
    this.sizes = List.copyOf(sizes);
    while (nameIter.hasNext() && md5Iter.hasNext()) {
      files.add(new MD5File(nameIter.next(), md5Iter.next()));
    }
  }

  public PublishMessage(List<String> names) throws IOException {
    init();
    for (String name : names) {
      files.add(new MD5File(name, md5(name)));
    }
  }

  private static void init() {
    if (digest == null) {
      try {
        digest = MessageDigest.getInstance("MD5");
      } catch (NoSuchAlgorithmException e) {
        logger.severe("Not supposed to happen");
        System.exit(-1);
      }
    }
  }

  @Override
  public ByteBuffer toByteBuffer() {
    final var buffer = ByteBuffer.allocate(BUFFER_SIZE)
        .putInt(ClientServerType.PUBLISH.id())
        .putInt(files.size());
    for (var file : files) {
      final var name = UTF_8.encode(file.name());
      buffer.putInt(name.remaining())
              .put(name)
              .put(file.md5().flip())
              .putInt(file.size());
      ;
    }
    return buffer;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof PublishMessage that)) return false;
    return Objects.equals(files, that.files);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(files);
  }

  @Override
  public String toString() {
    return "PublishMessage{" +
        files +
        '}';
  }

  public List<MD5File> getFiles() {
    return files;
  }

  private ByteBuffer md5(String file) throws IOException {
    final ByteBuffer buffer = ByteBuffer.allocate(MD5_SIZE * Byte.BYTES);
    try (FileChannel fc = FileChannel.open(Path.of(file), StandardOpenOption.READ)) {
      while (fc.read(buffer) > 0) {
        buffer.flip();
        digest.update(buffer);
        buffer.clear();
      }
    } catch (NoSuchFileException | FileNotFoundException e) {
      System.out.println("File not found.");
    }
    byte[] hash = digest.digest();
    var deepCopy = Arrays.copyOf(hash, MD5_SIZE); //DeepCopy needed because digest shenanigans
    // reset the digest
    digest.reset();
    ByteBuffer res = ByteBuffer.wrap(deepCopy);
    res.position(res.limit());
    return res;
  }
}
