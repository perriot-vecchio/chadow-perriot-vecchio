package fr.uge.net.chadow.parsing.messages;

import java.nio.ByteBuffer;

@FunctionalInterface
public interface Trame {
  ByteBuffer toByteBuffer();
}
