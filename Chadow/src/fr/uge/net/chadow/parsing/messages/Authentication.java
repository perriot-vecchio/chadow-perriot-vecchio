package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.nio.ByteBuffer;
import java.util.Objects;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public record Authentication(int listener, String login) implements Trame {

  public Authentication {
    Objects.requireNonNull(login);
  }

  @Override
  public ByteBuffer toByteBuffer() {
    var bytesLogin = UTF_8.encode(login);
    return ByteBuffer.allocate(BUFFER_SIZE)
      .putInt(ClientServerType.CONNEXION.id())
      .putInt(listener)
      .putInt(bytesLogin.remaining())
      .put(bytesLogin)
      ;
  }
}
