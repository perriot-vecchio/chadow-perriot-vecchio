package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.nio.ByteBuffer;
import java.util.Objects;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public record PrivateMessage(String sender, String receiver, String message) implements Trame {
  public PrivateMessage {
    Objects.requireNonNull(sender);
    Objects.requireNonNull(receiver);
    Objects.requireNonNull(message);
  }

  public ByteBuffer toByteBuffer() {
    var bytesSender = UTF_8.encode(sender);
    var bytesReceiver = UTF_8.encode(receiver);
    var bytesMessage = UTF_8.encode(message);
    return ByteBuffer.allocate(BUFFER_SIZE)
      .putInt(ClientServerType.PRIVATE.id())
      .putInt(bytesSender.remaining())
      .put(bytesSender)
      .putInt(bytesReceiver.remaining())
      .put(bytesReceiver)
      .putInt(bytesMessage.remaining())
      .put(bytesMessage);
  }
}
