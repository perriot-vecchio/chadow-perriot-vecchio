package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ServerClientType;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public class FileListMessage implements Trame {
  private static final String TITLE_LIST = "******List******\n";
  private final List<String> files;

  public FileListMessage(List<String> files) {
    this.files = List.copyOf(files);
  }

  public List<String> files() {
    return files;
  }

  @Override
  public ByteBuffer toByteBuffer() {
    ByteBuffer buff = ByteBuffer.allocate(BUFFER_SIZE)
        .putInt(ServerClientType.FILES_LIST.id()).putInt(files.size());
    for (String file : files) {
      var encoded = UTF_8.encode(file);
      buff.putInt(encoded.remaining()).put(encoded);
    }
    return buff;
  }

  @Override
  public String toString() {
    return TITLE_LIST + String.join("\n", files);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FileListMessage that = (FileListMessage) o;
    return files.equals(that.files);
  }

  @Override
  public int hashCode() {
    return Objects.hash(files);
  }
}
