package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public record RemoveMessage(List<String> files) implements Trame {

  public RemoveMessage {
    files = Objects.requireNonNullElse(files, Collections.emptyList());
  }

  @Override
  public ByteBuffer toByteBuffer() {
    final var buffer = ByteBuffer.allocate(BUFFER_SIZE)
        .putInt(ClientServerType.REMOVE.id())
        .putInt(files.size());
    for (String name : files) {
      final var file = UTF_8.encode(name);
      buffer.putInt(file.remaining()).put(file);
    }
    return buffer;
  }
}
