package fr.uge.net.chadow.parsing.messages;

import java.nio.ByteBuffer;

public class UploadMessage implements Trame {
  private final int sizeData;
  private final ByteBuffer data;

  public UploadMessage(int sizeData, ByteBuffer data) {
    this.sizeData = sizeData;
    this.data = data;
  }

  @Override
  public ByteBuffer toByteBuffer() {
    var bb = ByteBuffer.allocate(1024);
    bb.putInt(sizeData).put(data.flip());
    data.clear();
    return bb;
  }
}
