package fr.uge.net.chadow.parsing.messages;

import fr.uge.net.chadow.parsing.types.ClientServerType;

import java.nio.ByteBuffer;
import java.util.Objects;

import static fr.uge.net.chadow.Configuration.UTF_8;

public record AskMessage(byte mode, String fName) implements Trame {

  public AskMessage {
    Objects.requireNonNull(fName);
  }

  @Override
  public ByteBuffer toByteBuffer() {
    var bb = ByteBuffer.allocate(1024);
    var bytesFichier = UTF_8.encode(fName);
    bb.putInt(ClientServerType.ASKING.id()).put(mode).putInt(bytesFichier.remaining()).put(bytesFichier);
    return bb;
  }

  public boolean hidden() {
    return mode == (byte) 1;
  }
}
