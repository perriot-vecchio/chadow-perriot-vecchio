package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.Authentication;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;

public class AuthenticationReader implements Reader<Trame> {
  private enum State {
    WAITING_LISTENER, WAITING_LOGIN, DONE, ERROR;
  }

  private Authentication authentication;

  private State state = State.WAITING_LISTENER;
  private String login;
  private int listener;
  private final IntReader intReader = new IntReader();
  private final StringReader stringReader = new StringReader();

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    return switch (state) {
      case DONE, ERROR -> throw new IllegalStateException();
      case WAITING_LISTENER -> {
        var state = processListener(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_LOGIN -> processLogin(bb);
    };
  }

  private ProcessStatus processListener(ByteBuffer bb) {
    Reader.ProcessStatus intReaderStatus = intReader.process(bb);
    switch (intReaderStatus) {
      case DONE:
        listener = intReader.get();
        intReader.reset();
        state = State.WAITING_LOGIN;
        break;
      case REFILL:
        return ProcessStatus.REFILL;
      case ERROR:
        return ProcessStatus.ERROR;
    }
    return ProcessStatus.DONE;
  }

  private ProcessStatus processLogin(ByteBuffer buffer) {
    ProcessStatus status = stringReader.process(buffer);
    if (status != ProcessStatus.DONE) {
      return status;
    }
    login = stringReader.get();
    stringReader.reset();
    state = State.DONE;
    authentication = new Authentication(listener, login);
    return ProcessStatus.DONE;
  }

  @Override
  public Authentication get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return authentication;
  }

  @Override
  public void reset() {
    state = State.WAITING_LISTENER;
    intReader.reset();
    stringReader.reset();
  }
}
