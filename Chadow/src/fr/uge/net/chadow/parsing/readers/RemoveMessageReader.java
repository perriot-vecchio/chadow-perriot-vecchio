package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.RemoveMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class RemoveMessageReader implements Reader<Trame> {

  private enum State {
    WAITING_NBFILES, WAITING_FNAME, DONE, ERROR
  }

  private State state = State.WAITING_NBFILES;

  private final IntReader intReader = new IntReader();
  private final StringReader stringReader = new StringReader();
  private int nbFiles;
  private final List<String> files = new ArrayList<>();
  private RemoveMessage removeMessage;

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    return switch (state) {
      case DONE, ERROR -> throw new IllegalStateException();
      case WAITING_NBFILES -> {
        var state = processNbFiles(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_FNAME -> processFName(bb);
    };
  }


  private ProcessStatus processNbFiles(ByteBuffer bb) {
    ProcessStatus status = intReader.process(bb);
    if (status != ProcessStatus.DONE) {
      return status;
    }
    nbFiles = intReader.get();
    intReader.reset();
    state = State.WAITING_FNAME;
    return ProcessStatus.DONE;
  }

  private ProcessStatus processFName(ByteBuffer bb) {
    for (int i = 0; i < nbFiles; i++) {
      ProcessStatus status = stringReader.process(bb);
      if (status != ProcessStatus.DONE) {
        return status;
      }
      var v = stringReader.get();
      files.add(v);
      stringReader.reset();
      state = State.WAITING_NBFILES;
    }
    state = State.DONE;
    removeMessage = new RemoveMessage(files);
    return ProcessStatus.DONE;
  }

  @Override
  public Trame get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return removeMessage;
  }

  @Override
  public void reset() {
    state = State.WAITING_NBFILES;
    intReader.reset();
    stringReader.reset();
  }
}
