package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.CommandListMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;

public class ListCommandReader implements Reader<Trame> {
  @Override
  public ProcessStatus process(ByteBuffer bb) {
    return ProcessStatus.DONE;
  }

  @Override
  public Trame get() {
    return new CommandListMessage();
  }

  @Override
  public void reset() {
  }
}
