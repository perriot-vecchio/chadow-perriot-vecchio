package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.PublishMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static fr.uge.net.chadow.parsing.readers.Reader.ProcessStatus.DONE;

public class PublishMessageReader implements Reader<Trame> {

  enum State {
    WAITING_NBFILES, WAITING_FNAME, WAITING_FCONTENT, WAITING_FSIZE, DONE, ERROR
  }

  private static final int SIZE_MD5 = 16;
  private final ByteBuffer internalBuffer = ByteBuffer.allocate(SIZE_MD5);
  private final StringReader stringReader = new StringReader();
  private final IntReader intReader = new IntReader();
  private State state = State.WAITING_NBFILES;
  private int nbFiles;
  private List<String> nameList;
  private List<ByteBuffer> md5List;
  private List<Integer> fSizeList;
  private PublishMessage outputPublishMessage;

  @Override
  public ProcessStatus process(ByteBuffer bb) throws IOException {
    return switch (state) {
      case WAITING_NBFILES -> {
        var state = processNbFiles(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_FNAME -> {
        var state = processfName(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_FCONTENT -> {
        var state = processfContent(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_FSIZE -> {
        var state = processfSize(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case DONE -> ProcessStatus.DONE;
      case ERROR -> ProcessStatus.ERROR;
    };
  }

  private ProcessStatus processNbFiles(ByteBuffer bb) {
    switch (intReader.process(bb)) {
      case DONE:
        nbFiles = intReader.get();
        nameList = new ArrayList<>(nbFiles);
        md5List = new ArrayList<>(nbFiles);
        fSizeList = new ArrayList<>(nbFiles);
        intReader.reset();
        state = State.WAITING_FNAME;
        break;
      case REFILL:
        return ProcessStatus.REFILL;
      case ERROR:
        return ProcessStatus.ERROR;
    }
    return ProcessStatus.DONE;
  }

  private ProcessStatus processfName(ByteBuffer bb) {
    if (nbFiles > 0) {
      ProcessStatus status = stringReader.process(bb);
      if (status != DONE) {
        return status;
      }
      nameList.add(stringReader.get());
      stringReader.reset();
      state = State.WAITING_FCONTENT;
    } else {
      state = State.DONE;
    }
    return ProcessStatus.DONE;
  }

  private ProcessStatus processfContent(ByteBuffer buffer) throws IOException {
    if (nbFiles > 0) {
      buffer.flip();
      try {
        while (buffer.hasRemaining() && internalBuffer.hasRemaining()) {
          internalBuffer.put(buffer.get());
        }
        if (internalBuffer.hasRemaining()) {
          return ProcessStatus.REFILL;
        }
        state = State.WAITING_FSIZE;
        final var md5Buffer = ByteBuffer.allocate(SIZE_MD5);
        md5List.add(md5Buffer.put(internalBuffer.flip()));
        internalBuffer.clear(); // maybe not needed
      } finally {
        buffer.compact();
      }
    } else {
      state = State.DONE;
    }
    outputPublishMessage = new PublishMessage(nameList, md5List, fSizeList);
    return ProcessStatus.DONE;
  }

  private ProcessStatus processfSize(ByteBuffer bb) {
    if (nbFiles > 0) {
      switch (intReader.process(bb)) {
        case DONE:
          fSizeList.add(intReader.get());
          intReader.reset();
          state = State.WAITING_FNAME;
          nbFiles--;
          break;
        case REFILL:
          return ProcessStatus.REFILL;
        case ERROR:
          return ProcessStatus.ERROR;
      }
    } else {
      state = State.DONE;
    }
    return ProcessStatus.DONE;
  }


  @Override
  public PublishMessage get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return outputPublishMessage;
  }

  @Override
  public void reset() {
    state = State.WAITING_NBFILES;
    nameList.clear();
    md5List.clear();
    fSizeList.clear();
    intReader.reset();
    stringReader.reset();
    internalBuffer.clear();
  }
}
