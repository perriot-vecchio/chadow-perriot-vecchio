package fr.uge.net.chadow.parsing.readers;

import java.nio.ByteBuffer;

public class ByteReader implements Reader<Byte> {
  byte value;
  private State state = State.WAITING;

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    if (state == State.DONE) {
      return ProcessStatus.DONE;
    }
    bb.flip();
    try {
      if (!bb.hasRemaining()) {
        return ProcessStatus.REFILL;
      }
      value = bb.get();
      state = State.DONE;
      return ProcessStatus.DONE;
    } finally {
      bb.compact();
    }
  }

  @Override
  public Byte get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return value;
  }

  @Override
  public void reset() {
    state = State.WAITING;
  }

  private enum State {
    WAITING, DONE
  }
}
