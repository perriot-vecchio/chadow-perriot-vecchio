package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.PrivateMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;

public class PrivateMessageReader implements Reader<Trame> {

  private enum State {
    WAITING_SENDER, WAITING_NEXT, DONE, ERROR
  }

  private State state = State.WAITING_SENDER;

  private final StringReader stringReader = new StringReader();

  private final MessageReader messageReader = new MessageReader();
  String sender;

  PrivateMessage privateMessage;

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    return switch (state) {
      case DONE, ERROR -> throw new IllegalStateException();
      case WAITING_SENDER -> {
        var state = processSender(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_NEXT -> processMessage(bb);
    };
  }


  private ProcessStatus processSender(ByteBuffer bb) {
    ProcessStatus status = stringReader.process(bb);
    if (status != ProcessStatus.DONE) {
      return status;
    }
    sender = stringReader.get();
    stringReader.reset();
    state = State.WAITING_NEXT;
    return ProcessStatus.DONE;
  }

  private ProcessStatus processMessage(ByteBuffer bb) {
    ProcessStatus status = messageReader.process(bb);
    switch (status) {
      case DONE -> {
        privateMessage = new PrivateMessage(sender, messageReader.get().login(), messageReader.get().message());
        messageReader.reset();
        state = State.DONE;
        return ProcessStatus.DONE;
      }
      case REFILL -> {
        return ProcessStatus.REFILL;
      }
      case ERROR -> {
        return ProcessStatus.ERROR;
      }
      default -> throw new IllegalStateException(); // It shouldn't happen, but it's a quirk of the compiler
    }
  }

  @Override
  public PrivateMessage get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return privateMessage;
  }

  @Override
  public void reset() {
    state = State.WAITING_SENDER;
    stringReader.reset();
    messageReader.reset();
  }
}
