package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.SharingMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;

public final class SharingReader implements Reader<Trame> {
  private final StringReader stringReader = new StringReader();
  private final IntReader intReader = new IntReader();
  private final ByteReader byteReader = new ByteReader();
  private State state = State.WAITING_ID;
  private int transactionId;
  private String fileName;
  private String pseudo;


  @Override
  public ProcessStatus process(ByteBuffer bb) {
    return switch (state) {
      case WAITING_ID -> {
        final var processStatus = intReader.process(bb);
        if (processStatus != ProcessStatus.DONE) {
          yield processStatus;
        }
        transactionId = intReader.get();
        state = State.WAITING_FILE;
        intReader.reset();
        yield process(bb);
      }
      case WAITING_FILE -> {
        final var processStatus = stringReader.process(bb);
        if (processStatus != ProcessStatus.DONE) {
          yield processStatus;
        }
        fileName = stringReader.get();
        state = State.WAITING_MODE;
        stringReader.reset();
        yield process(bb);
      }
      case WAITING_MODE -> {
        final var processStatus = byteReader.process(bb);
        if (processStatus != ProcessStatus.DONE) {
          yield processStatus;
        }
        final var mode = byteReader.get();
        state = (mode == (byte) 1) ? State.DONE : State.WAITING_PSEUDO;
        byteReader.reset();
        yield process(bb);
      }
      case WAITING_PSEUDO -> {
        final var processStatus = stringReader.process(bb);
        if (processStatus != ProcessStatus.DONE) {
          yield processStatus;
        }
        pseudo = stringReader.get();
        stringReader.reset();
        state = State.DONE;
        yield process(bb);
      }
      default -> (state == State.DONE) ? ProcessStatus.DONE : ProcessStatus.ERROR;
    };
  }

  @Override
  public Trame get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    if (pseudo == null) {
      return new SharingMessage(fileName, transactionId);
    }
    return new SharingMessage(fileName, transactionId, pseudo);
  }

  @Override
  public void reset() {
    state = State.WAITING_ID;
    pseudo = null;
  }

  private enum State {
    WAITING_ID, WAITING_FILE, WAITING_MODE, WAITING_PSEUDO, DONE, ERROR
  }
}
