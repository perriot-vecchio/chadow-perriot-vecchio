package fr.uge.net.chadow.parsing.readers;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface Reader<T> {

  ProcessStatus process(ByteBuffer bb) throws IOException;

  T get();

  void reset();

  enum ProcessStatus {DONE, REFILL, ERROR}

}
