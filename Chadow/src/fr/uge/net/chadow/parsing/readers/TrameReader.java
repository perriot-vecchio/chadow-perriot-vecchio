package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.Trame;
import fr.uge.net.chadow.parsing.types.MessageType;

import java.io.IOException;
import java.nio.ByteBuffer;

public class TrameReader implements Reader<Trame> {
  private final IdReader idReader;
  private State state = State.WAITING_ID;
  private MessageType id;
  private Trame trame;

  public TrameReader(Class<? extends MessageType> idReaderClass) {
    this.idReader = new IdReader(idReaderClass);
  }


  @Override
  public ProcessStatus process(ByteBuffer bb) throws IOException {
    return switch (state) {
      case WAITING_ID -> {
        final var processStatus = idReader.process(bb);
        if (processStatus == ProcessStatus.DONE) {
          state = State.WAITING_MESSAGE;
          id = idReader.get();
          idReader.reset();
          yield process(bb);
        }
        yield processStatus;
      }
      case WAITING_MESSAGE -> processInMessage(bb);
      case DONE -> ProcessStatus.DONE;
      case ERROR -> ProcessStatus.ERROR;
    };
  }

  private ProcessStatus processInMessage(ByteBuffer bb) throws IOException {
    final var trameReader = id.reader();
    final var processStatus = trameReader.process(bb);
    if (processStatus == ProcessStatus.DONE) {
      state = State.DONE;
      trame = trameReader.get();
      trameReader.reset();
    }
    return processStatus;
  }

  @Override
  public Trame get() {
    return trame;
  }

  @Override
  public void reset() {
    state = State.WAITING_ID;
  }

  private enum State {
    WAITING_ID, WAITING_MESSAGE, DONE, ERROR
  }
}
