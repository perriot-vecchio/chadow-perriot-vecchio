package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.types.MessageType;

import java.nio.ByteBuffer;

public class IdReader implements Reader<MessageType> {
  private final Class<? extends MessageType> type;
  private final IntReader intReader;
  private State state = State.WAITING;
  private int id;

  public IdReader(Class<? extends MessageType> type) {
    this.type = type;
    this.intReader = new IntReader();
  }

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    if (state != State.WAITING) {
      return state == State.DONE ? ProcessStatus.DONE : ProcessStatus.ERROR;
    }
    var processStatus = intReader.process(bb);
    if (processStatus == ProcessStatus.DONE) {
      state = State.DONE;
      id = intReader.get();
      intReader.reset();
    }
    return processStatus;
  }

  @Override
  public MessageType get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return type.getEnumConstants()[id];
  }

  @Override
  public void reset() {
    state = State.WAITING;
  }

  private enum State {
    WAITING, DONE, ERROR
  }
}
