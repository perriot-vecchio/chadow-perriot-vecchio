package fr.uge.net.chadow.parsing.readers;

import java.nio.ByteBuffer;

import static fr.uge.net.chadow.Configuration.UTF_8;

public class StringReader implements Reader<String> {

  private enum State {
    SIZE, MESSAGE, DONE, ERROR
  }

  private static final int SIZE_MAX = 1024;
  private final ByteBuffer internalBuffer = ByteBuffer.allocateDirect(SIZE_MAX);
  private State state = State.SIZE;
  private String message;
  private final IntReader intReader = new IntReader();
  private int size = -1;
  private int seen = 0;

  private ProcessStatus processSize(ByteBuffer buffer) {
    Reader.ProcessStatus intReaderStatus = intReader.process(buffer);
    switch (intReaderStatus) {
      case DONE:
        size = intReader.get();
        intReader.reset();
        state = State.MESSAGE;
        break;
      case REFILL:
        return ProcessStatus.REFILL;
      case ERROR:

        return ProcessStatus.ERROR;
    }
    if (size < 0 || size > SIZE_MAX) {
      return ProcessStatus.ERROR;
    }
    state = State.MESSAGE;
    return ProcessStatus.DONE;
  }

  private ProcessStatus processMessage(ByteBuffer buffer) {
    buffer.flip();
    try {
      while (buffer.hasRemaining() && internalBuffer.hasRemaining() && seen < size) {
        internalBuffer.put(buffer.get());
        seen++;
      }
      if (seen < size) {
        return ProcessStatus.REFILL;
      }
      state = State.DONE;
      seen = 0;
      message = UTF_8.decode(internalBuffer.flip()).toString();
      internalBuffer.compact();
      return ProcessStatus.DONE;
    } finally {
      buffer.compact();
    }
  }

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    return switch (state) {
      case DONE, ERROR -> throw new IllegalStateException();
      case MESSAGE -> processMessage(buffer);
      case SIZE -> {
        var state = processSize(buffer);
        yield state == ProcessStatus.DONE ? process(buffer) : state;
      }
    };
  }

  @Override
  public String get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return message;
  }

  @Override
  public void reset() {
    state = State.SIZE;
    internalBuffer.clear();
  }
}