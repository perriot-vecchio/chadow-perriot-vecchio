package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.PendingTransactionMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;

public class PendingTransactionMessageReader implements Reader<Trame> {

  enum State {
    WAITING_BYTE, WAITING_TRANSACTIONID, DONE, ERROR
  }

  private final IntReader intReader = new IntReader();
  private final ByteReader byteReader = new ByteReader();
  private int transactionID;
  private byte accept;

  private State state = State.WAITING_BYTE;

  @Override
  public Reader.ProcessStatus process(ByteBuffer bb) {
    return switch (state) {
      case WAITING_BYTE -> {
        final var processStatus = byteReader.process(bb);
        if (processStatus != ProcessStatus.DONE) {
          yield processStatus;
        }
        accept = byteReader.get();
        state = State.WAITING_TRANSACTIONID;
        byteReader.reset();
        yield process(bb);
      }
      case WAITING_TRANSACTIONID -> {
        final var processStatus = intReader.process(bb);
        if (processStatus != Reader.ProcessStatus.DONE) {
          yield processStatus;
        }
        transactionID = intReader.get();
        state = State.DONE;
        intReader.reset();
        yield process(bb);
      }

      default -> (state == State.DONE) ? Reader.ProcessStatus.DONE : Reader.ProcessStatus.ERROR;
    };
  }

  @Override
  public Trame get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new PendingTransactionMessage(accept, transactionID);
  }

  @Override
  public void reset() {
    state = State.WAITING_BYTE;
    intReader.reset();
  }
}
