package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.FileListMessage;
import fr.uge.net.chadow.parsing.messages.PrivateMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static fr.uge.net.chadow.parsing.readers.Reader.ProcessStatus.DONE;

public class FileListReader implements Reader<Trame> {

  private enum State {
    WAITING_NB_FILES, WAITING_FNAME, DONE, ERROR
  }

  private State state = State.WAITING_NB_FILES;

  private final IntReader nbFileReader = new IntReader();

  private int nbFiles;

  private final StringReader stringReader = new StringReader();


  private final List<String> files = new ArrayList<>();

  @Override
  public ProcessStatus process(ByteBuffer bb) {

    return switch (state) {
      case ERROR -> throw new IllegalStateException();
      case WAITING_NB_FILES -> {
        var state = processNbFiles(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_FNAME -> {
        var state = processfName(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case DONE -> ProcessStatus.DONE;
    };
  }


  private ProcessStatus processNbFiles(ByteBuffer bb) {
    switch (nbFileReader.process(bb)) {
      case DONE:
        nbFiles = nbFileReader.get();
        nbFileReader.reset();
        state = State.WAITING_FNAME;
        break;
      case REFILL:
        return ProcessStatus.REFILL;
      case ERROR:
        return ProcessStatus.ERROR;
    }
    return ProcessStatus.DONE;
  }

  private ProcessStatus processfName(ByteBuffer bb) {
    if (nbFiles > 0) {
      ProcessStatus status = stringReader.process(bb);
      if (status != DONE) {
        return status;
      }
      files.add(stringReader.get());
      stringReader.reset();
      nbFiles--;
      state = State.WAITING_FNAME;
    } else {
      state = State.DONE;
    }
    return ProcessStatus.DONE;
  }

  @Override
  public FileListMessage get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new FileListMessage(files);
  }

  @Override
  public void reset() {
    state = State.WAITING_NB_FILES;
    files.clear();
    nbFileReader.reset();
    stringReader.reset();
  }
}
