package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.AskMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;

public class AskMessageReader implements Reader<Trame> {

  private enum State {
    DONE,
    WAITING_MODE,
    WAITING_FNAME,
    ERROR
  }

  State state = State.WAITING_MODE;

  byte mode;

  AskMessage askMessage;

  private final StringReader stringReader = new StringReader();

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    return switch (state) {
      case DONE, ERROR -> throw new IllegalStateException();
      case WAITING_MODE -> {
        var state = processMode(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_FNAME -> processfName(bb);
    };
  }

  private ProcessStatus processMode(ByteBuffer bb) {
    bb.flip();
    try {
      if (bb.remaining() < 1) {
        return ProcessStatus.REFILL;
      }
      mode = bb.get();
      state = State.WAITING_FNAME;
      return ProcessStatus.DONE;
    } finally {
      bb.compact();
    }
  }

  private ProcessStatus processfName(ByteBuffer bb) {
    ProcessStatus status = stringReader.process(bb);
    if (status != ProcessStatus.DONE) {
      return status;
    }
    askMessage = new AskMessage(mode, stringReader.get());
    stringReader.reset();
    state = State.DONE;
    return ProcessStatus.DONE;
  }
  @Override
  public Trame get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return askMessage;
  }

  @Override
  public void reset() {
    state = State.WAITING_MODE;
    stringReader.reset();
  }
}
