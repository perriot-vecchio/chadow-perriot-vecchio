package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.PublicMessage;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.nio.ByteBuffer;

public class MessageReader implements Reader<Trame> {
  private enum State {
    WAITING_LOGIN, WAITING_TEXT, DONE, ERROR
  }

  private State state = State.WAITING_LOGIN;

  private PublicMessage message;
  private String login;
  private final StringReader stringReader = new StringReader();


  @Override
  public ProcessStatus process(ByteBuffer bb) {
    return switch (state) {
      case DONE, ERROR -> throw new IllegalStateException();
      case WAITING_LOGIN -> {
        var state = processLogin(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_TEXT -> processText(bb);
    };
  }


  private ProcessStatus processLogin(ByteBuffer buffer) {
    ProcessStatus status = stringReader.process(buffer);
    if (status != ProcessStatus.DONE) {
      return status;
    }
    login = stringReader.get();
    stringReader.reset();
    state = State.WAITING_TEXT;
    return ProcessStatus.DONE;
  }

  private ProcessStatus processText(ByteBuffer buffer) {
    ProcessStatus status = stringReader.process(buffer);
    switch (status) {
      case DONE -> {
        message = new PublicMessage(login, stringReader.get());
        stringReader.reset();
        state = State.DONE;
        return ProcessStatus.DONE;
      }
      case REFILL -> {
        return ProcessStatus.REFILL;
      }
      case ERROR -> {
        return ProcessStatus.ERROR;
      }
      default -> throw new IllegalStateException(); // It shouldn't happen, but it's a quirk of the compiler
    }
  }

  @Override
  public PublicMessage get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return message;
  }

  @Override
  public void reset() {
    state = State.WAITING_LOGIN;
  }
}
