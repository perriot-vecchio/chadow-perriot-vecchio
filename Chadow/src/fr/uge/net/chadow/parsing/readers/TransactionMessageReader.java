package fr.uge.net.chadow.parsing.readers;

import fr.uge.net.chadow.parsing.messages.Trame;
import fr.uge.net.chadow.parsing.messages.TransactionMessage;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static fr.uge.net.chadow.parsing.readers.Reader.ProcessStatus.DONE;

public class TransactionMessageReader implements Reader<Trame> {


  private String fileName;

  private State state = State.WAITING_FSIZE;
  private final IntReader intReader = new IntReader();
  private final List<String> addresses = new ArrayList<>();
  private final List<Integer> ports = new ArrayList<>();
  private final StringReader stringReader = new StringReader();
  private int fSize;
  private int nbPersons;

  @Override
  public ProcessStatus process(ByteBuffer bb) {

    return switch (state) {
      case ERROR -> throw new IllegalStateException();
      case WAITING_FSIZE -> {
        var state = processfSize(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_FILE_NAME -> {
        final var processStatus = stringReader.process(bb);
        if (processStatus != DONE) {
          yield processStatus;
        }
        fileName = stringReader.get();
        stringReader.reset();
        state = State.WAITING_NB_PERSONS;
        yield process(bb);
      }
      case WAITING_NB_PERSONS -> {
        var state = processNbPersons(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_ADRESSES -> {
        var state = processfName(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case WAITING_PORTS -> {
        var state = processPort(bb);
        yield state == ProcessStatus.DONE ? process(bb) : state;
      }
      case DONE -> ProcessStatus.DONE;
    };
  }

  private ProcessStatus processfSize(ByteBuffer bb) {
    switch (intReader.process(bb)) {
      case DONE:
        fSize = intReader.get();
        intReader.reset();
        state = State.WAITING_FILE_NAME;
        break;
      case REFILL:
        return ProcessStatus.REFILL;
      case ERROR:
        return ProcessStatus.ERROR;
    }
    return ProcessStatus.DONE;
  }

  @Override
  public TransactionMessage get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    final List<InetSocketAddress> socketAddresses = new ArrayList<>();
    for (int i = 0; i < addresses.size(); i++) {
      socketAddresses.add(new InetSocketAddress(addresses.get(i), ports.get(i)));
    }
    return new TransactionMessage(fSize, fileName, socketAddresses);
  }

  private ProcessStatus processNbPersons(ByteBuffer bb) {
    switch (intReader.process(bb)) {
      case DONE:
        nbPersons = intReader.get();
        intReader.reset();
        state = State.WAITING_ADRESSES;
        break;
      case REFILL:
        return ProcessStatus.REFILL;
      case ERROR:
        return ProcessStatus.ERROR;
    }
    return ProcessStatus.DONE;
  }

  private ProcessStatus processfName(ByteBuffer bb) {
    if (nbPersons > 0) {
      ProcessStatus status = stringReader.process(bb);
      if (status != DONE) {
        return status;
      }
      addresses.add(stringReader.get());
      stringReader.reset();
      state = State.WAITING_PORTS;
    } else {
      state = State.WAITING_PORTS;
    }
    return ProcessStatus.DONE;
  }

  private ProcessStatus processPort(ByteBuffer bb) {
    if (nbPersons > 0) {
      switch (intReader.process(bb)) {
        case DONE:
          ports.add(intReader.get());
          intReader.reset();
          state = State.WAITING_ADRESSES;
          nbPersons--;
          break;
        case REFILL:
          return ProcessStatus.REFILL;
        case ERROR:
          return ProcessStatus.ERROR;
      }
    } else {
      state = State.DONE;
    }
    return ProcessStatus.DONE;
  }

  private enum State {
    WAITING_FSIZE, WAITING_FILE_NAME, WAITING_NB_PERSONS, WAITING_ADRESSES, WAITING_PORTS, DONE, ERROR
  }

  @Override
  public void reset() {
    state = State.WAITING_FSIZE;
    addresses.clear();
    ports.clear();
    intReader.reset();
    stringReader.reset();
  }


}
