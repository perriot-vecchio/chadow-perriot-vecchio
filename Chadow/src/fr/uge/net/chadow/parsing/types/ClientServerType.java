package fr.uge.net.chadow.parsing.types;

import fr.uge.net.chadow.parsing.messages.Trame;
import fr.uge.net.chadow.parsing.readers.*;

public enum ClientServerType implements MessageType {
  ALL(new MessageReader()),
  PRIVATE(new PrivateMessageReader()),
  PUBLISH(new PublishMessageReader()),
  REMOVE(new RemoveMessageReader()),
  LIST(new ListCommandReader()),
  ASKING(new AskMessageReader()),
  CONNEXION(new AuthenticationReader()),
  PROXY,
  PENDING_TRANSACTION(new PendingTransactionMessageReader());

  private final Reader<Trame> reader;

  ClientServerType(Reader<Trame> reader) {
    this.reader = reader;
  }

  ClientServerType() {
    this.reader = null;
  } // TODO : Need to disappear

  @Override
  public int id() {
    return ordinal();
  }

  @Override
  public Reader<Trame> reader() {
    return reader;
  }
}
