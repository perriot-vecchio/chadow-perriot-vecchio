package fr.uge.net.chadow.parsing.types;

import fr.uge.net.chadow.parsing.messages.Trame;
import fr.uge.net.chadow.parsing.readers.*;

public enum ServerClientType implements MessageType {
  MESSAGE(new MessageReader()),
  RENAME,
  FILES_LIST(new FileListReader()),
  SHARING(new SharingReader()),
  ABORT,
  TRANSACTION(new TransactionMessageReader()),
  PROXY,
  ;

  private final Reader<Trame> reader;

  ServerClientType(Reader<Trame> reader) {
    this.reader = reader;
  }

  ServerClientType() {
    this.reader = null;
  }

  @Override
  public int id() {
    return this.ordinal();
  }

  @Override
  public Reader<Trame> reader() {
    return reader;
  }
}
