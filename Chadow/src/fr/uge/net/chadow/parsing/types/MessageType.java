package fr.uge.net.chadow.parsing.types;

import fr.uge.net.chadow.parsing.messages.Trame;
import fr.uge.net.chadow.parsing.readers.Reader;

public interface MessageType {
  int id();

  Reader<Trame> reader();
}
