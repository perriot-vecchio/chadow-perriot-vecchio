package fr.uge.net.chadow.server;

import fr.uge.net.chadow.contexts.Context;
import fr.uge.net.chadow.parsing.messages.TransactionMessage;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Transaction {
  private final String fileName;
  private final Context target;
  private final int nbParticipants;
  private final int fileSize;
  private final Map<InetSocketAddress, Boolean> approvals;

  public Transaction(String fileName, Context target, int nbParticipants, int fileSize) {
    this.fileName = fileName;
    this.target = target;
    this.nbParticipants = nbParticipants;
    this.fileSize = fileSize;
    approvals = new HashMap<>();
  }

  public void register(Approval... newApprovals) {
    for (final var newApproval : newApprovals) {
      approvals.put(newApproval.address, newApproval.approve);
    }
  }

  private List<InetSocketAddress> providers() {
    return approvals.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).toList();
  }

  public boolean isCompleted() {
    return nbParticipants == approvals.size();
  }

  public void sendMessage() {
    target.queueMessage(new TransactionMessage(fileSize, fileName, providers()));
  }

  public record Approval(InetSocketAddress address, boolean approve) {
    public Approval {
      Objects.requireNonNull(address);
    }
  }
}
