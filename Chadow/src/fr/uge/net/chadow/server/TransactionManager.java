package fr.uge.net.chadow.server;

import fr.uge.net.chadow.contexts.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TransactionManager {
  private final Map<Integer, Transaction> pendingTransactions;
  private int nbTransactions;

  public TransactionManager() {
    nbTransactions = 0;
    this.pendingTransactions = new HashMap<>();
  }

  public int addTransaction(Context cible, int nbParticipant, String fileName, int fileSize) {
    final var id = nbTransactions++;
    pendingTransactions.put(id, new Transaction(fileName, cible, nbParticipant, fileSize));
    return id;
  }

  public Optional<Transaction> answer(int id, Transaction.Approval... approvals) {
    final var transaction = pendingTransactions.get(id);
    if (transaction == null) {
      return Optional.empty();
    }
    transaction.register(approvals);
    return Optional.of(transaction);
  }

  public void completedTransaction(int id) {
    pendingTransactions.remove(id);
  }
}
