package fr.uge.net.chadow.server;


import fr.uge.net.chadow.contexts.Context;
import fr.uge.net.chadow.contexts.ServerContext;
import fr.uge.net.chadow.parsing.messages.Trame;

import javax.swing.text.html.Option;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Server {
  private static final Logger logger = Logger.getLogger(Server.class.getName());
  private final ServerSocketChannel serverSocketChannel;
  private final Selector selector;
  public final FileManager fileManager = new FileManager();
  public final TransactionManager transactionManager = new TransactionManager();

  public Server(int port) throws IOException {
    serverSocketChannel = ServerSocketChannel.open();
    serverSocketChannel.bind(new InetSocketAddress(port));
    selector = Selector.open();
  }

  public void launch() throws IOException {
    serverSocketChannel.configureBlocking(false);
    serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
    while (!Thread.interrupted()) {
      Helpers.printKeys(selector); // for debug
      System.out.println("Starting select");
      try {
        selector.select(this::treatKey);
      } catch (UncheckedIOException tunneled) {
        throw tunneled.getCause();
      }
      System.out.println("Select finished");
    }
  }

  public static void main(String[] args)
      throws NumberFormatException, IOException {
    if (args.length != 1) {
      usage();
      return;
    }
    new Server(Integer.parseInt(args[0])).launch();
  }

  private void treatKey(SelectionKey key) throws UncheckedIOException {
    Helpers.printSelectedKey(key); // for debug
    try {
      if (key.isValid() && key.isAcceptable()) {
        doAccept(key);
      }
    } catch (IOException ioe) {
      // lambda call in select requires to tunnel IOException
      throw new UncheckedIOException(ioe);
    }
    try {
      final var context = (Context) key.attachment();
      if (key.isValid() && key.isWritable()) {
        context.doWrite();
      }
      if (key.isValid() && key.isReadable()) {
        context.doRead();
      }
    } catch (IOException e) {
      logger.log(Level.INFO, "Connection closed with client due to IOException", e); //FIXME: remove this later
      silentlyClose(key);
    }
  }

  private void silentlyClose(SelectionKey key) {
    Channel sc = key.channel();
    Optional.ofNullable((ServerContext) key.attachment()).ifPresent(ServerContext::silentlyClose);
    try {
      sc.close();
    } catch (IOException e) {
      // ignore exception
    }
  }

  /**
   * Add a message to all connected clients queue
   */
  public void broadcast(Trame msg) {
    for (var key : selector.keys()) {
      var context = (Context) key.attachment();
      if (context != null) {
        context.queueMessage(msg);
      }
    }
  }

  public Optional<Context> findContextByPseudo(String pseudo) {
    for (var key : selector.keys()) {
      var context = (ServerContext) key.attachment();
      if (context != null && pseudo.equals(context.pseudo())) {
        return Optional.of(context);
      }
    }
    return Optional.empty();
  }

  private void doAccept(SelectionKey key) throws IOException {
    ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
    SocketChannel sc = ssc.accept();
    if (sc == null) {
      return;
    }
    sc.configureBlocking(false);
    final var contextKey = sc.register(selector, SelectionKey.OP_READ);
    contextKey.attach(new ServerContext(this, contextKey));
  }

  private static void usage() {
    System.out.println("Usage : server port");
  }

  public Optional<ServerContext> findContextByUUID(UUID uuid) {
    for (var key : selector.keys()) {
      var context = (ServerContext) key.attachment();
      if (context != null && uuid.equals(context.uuid())) {
        return Optional.of(context);
      }
    }
    return Optional.empty();
  }
}
