package fr.uge.net.chadow.server;

import fr.uge.net.chadow.parsing.messages.MD5File;

import java.util.*;

public class FileManager {

  private final Map<MD5File, Set<UUID>> ownersByFile;

  public FileManager() {
    this.ownersByFile = new HashMap<>();
  }

  public void addFile(MD5File file, UUID owner) {
    ownersByFile.merge(file, new HashSet<>(Set.of(owner)), (old, newOwner) -> {
      old.addAll(newOwner);
      return old;
    });
  }

  public void addFiles(List<MD5File> files, UUID owner) {
    files.forEach(file -> addFile(file, owner));
    System.err.println(ownersByFile);
  }

  public List<String> files() {
    return ownersByFile.keySet().stream().map(MD5File::name).toList();
  }


  public void removeFile(String file, UUID uuid) {
    ownersByFile.entrySet().removeIf(entry -> {
      final var fileName = entry.getKey().name();
      if (file.equals(fileName)) {
        entry.getValue().remove(uuid);
      }
      return entry.getValue().isEmpty();
    });
  }


  public void removeFiles(List<String> files, UUID uuid) {
    files.forEach(file -> removeFile(file, uuid));
  }


  public void removeUUIDForFile(UUID uuid) {
    ownersByFile.entrySet().removeIf(entry -> {
      entry.getValue().remove(uuid);
      return entry.getValue().isEmpty();
    });
  }

  public Set<UUID> getOwners(String file) {
    return ownersByFile.entrySet().stream()
            .filter(entry -> entry.getKey().name().equals(file))
            .map(Map.Entry::getValue)
            .findAny().orElse(Collections.emptySet())
            ;
  }


  public int getFileSize(String s) {
    return ownersByFile.keySet().stream()
            .filter(file -> file.name().equals(s))
            .map(MD5File::size)
            .findAny().orElse(-1);
  }
}
