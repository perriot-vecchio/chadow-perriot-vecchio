package fr.uge.net.chadow.contexts;

import fr.uge.net.chadow.parsing.messages.Trame;

import java.io.IOException;

sealed public interface Context permits ClientContext, DownloadContext, ServerContext, UploadContext {

  /**
   * Add a message to the message queue, tries to fill bufferOut and updateInterestOps
   *
   * @param msg
   */
  void queueMessage(Trame msg);

  /**
   * Process the content of bufferIn
   * <p>
   * The convention is that bufferIn is in write-mode before the call to process
   * and after the call
   */
  void processIn() throws IOException;

  /**
   * Try to fill bufferOut from the message queue
   */
  void processOut() throws IOException;

  /**
   * Performs the write action on sc
   * <p>
   * The convention is that both buffers are in write-mode before the call to
   * doWrite and after the call
   *
   * @throws IOException
   */
  void doWrite() throws IOException;

  /**
   * Performs the read action on sc
   * <p>
   * The convention is that both buffers are in write-mode before the call to
   * doRead and after the call
   *
   * @throws IOException
   */
  void doRead() throws IOException;

  /**
   * Update the interestOps of the key looking only at values of the boolean
   * closed and of both ByteBuffers.
   * <p>
   * The convention is that both buffers are in write-mode before the call to
   * updateInterestOps and after the call. Also it is assumed that process has
   * been be called just before updateInterestOps.
   */
  void updateInterestOps();

  default void doConnect() throws IOException {

  }
}
