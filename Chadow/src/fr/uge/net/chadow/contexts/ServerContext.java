package fr.uge.net.chadow.contexts;

import fr.uge.net.chadow.parsing.messages.*;
import fr.uge.net.chadow.parsing.readers.Reader;
import fr.uge.net.chadow.parsing.readers.TrameReader;
import fr.uge.net.chadow.parsing.types.ClientServerType;
import fr.uge.net.chadow.server.FileManager;
import fr.uge.net.chadow.server.Server;
import fr.uge.net.chadow.server.Transaction;
import fr.uge.net.chadow.server.TransactionManager;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;

public final class ServerContext implements Context {
  private static final Logger logger = Logger.getLogger(ServerContext.class.getName());
  private final SelectionKey key;
  private final SocketChannel sc;
  private final ByteBuffer bufferIn;
  private final ByteBuffer bufferOut;
  private final Deque<Trame> queue;
  private final Server server;
  private final TrameReader trameReader;
  private final FileManager fileManager;
  private final TransactionManager transactionManager;
  private final UUID uuid;
  private boolean closed;
  private String pseudo;
  private int listeningPort;

  public ServerContext(Server server, SelectionKey key) {
    this.key = key;
    this.server = server;
    sc = (SocketChannel) key.channel();
    bufferIn = ByteBuffer.allocate(BUFFER_SIZE);
    bufferOut = ByteBuffer.allocate(BUFFER_SIZE);
    queue = new ArrayDeque<>();
    trameReader = new TrameReader(ClientServerType.class);
    fileManager = server.fileManager;
    transactionManager = server.transactionManager;
    uuid = UUID.randomUUID();
    closed = false;
  }

  @Override
  public void processIn() throws IOException {
    for (; ; ) {
      Reader.ProcessStatus status = trameReader.process(bufferIn);
      if (status == Reader.ProcessStatus.ERROR) {
        silentlyClose();
      }
      if (status != Reader.ProcessStatus.DONE) {
        return;
      }
      final var msg = trameReader.get();
      switch (msg) {
        case Authentication connexion -> {
          pseudo = connexion.login();
          listeningPort = connexion.listener();
        }
        case PublicMessage publicMessage -> {
          pseudo = publicMessage.login();
          server.broadcast(publicMessage);
        }
        case PrivateMessage privateMessage -> {
          pseudo = privateMessage.sender();
          server.findContextByPseudo(privateMessage.receiver())
                  .ifPresent(context -> context.queueMessage(new PublicMessage(pseudo, privateMessage.message())));
        }
        case CommandListMessage _ -> queueMessage(new FileListMessage(fileManager.files()));
        case PublishMessage publishMessage ->
                fileManager.addFiles(publishMessage.getFiles(), uuid); // TODO Rename if same name but != MD5.
        case RemoveMessage removeMessage -> {
          fileManager.removeFiles(removeMessage.files(), uuid);
        }
        case AskMessage askMessage -> {
          final var ownersContext = fileManager.getOwners(askMessage.fName())
                  .stream()
                  .map(server::findContextByUUID)
                  .flatMap(Optional::stream)
                  .toList();

          //retrieve the the size of the file
          final var size = fileManager.getFileSize(askMessage.fName());

          final var transactionId = transactionManager.addTransaction(this, ownersContext.size(), askMessage.fName(), size);
          final var newMessage = askMessage.hidden()
                  ? new SharingMessage(askMessage.fName(), transactionId)
                  : new SharingMessage(askMessage.fName(), transactionId, pseudo);

          ownersContext.forEach(serverContext -> serverContext.queueMessage(newMessage));
        }
        case PendingTransactionMessage pendingTransactionMessage -> {
          final var id = pendingTransactionMessage.id();
          final var answered = transactionManager.answer(id, new Transaction.Approval(
              getDownloadAddress(), pendingTransactionMessage.hasAccepted()));

          answered.ifPresent(transaction -> {
            if (transaction.isCompleted()) {
              transactionManager.completedTransaction(id);
              transaction.sendMessage();
            }
          });
        }
        default -> throw new IllegalStateException("Unexpected value: " + msg);
      }
      trameReader.reset();
      break;
    }
  }

  @Override
  public void queueMessage(Trame msg) {
    queue.add(msg);
    processOut();
    updateInterestOps();
  }

  @Override
  public void processOut() {
    while (!queue.isEmpty()) {
      var msg = queue.peek();
      var msgBytes = msg.toByteBuffer().flip();
      if (msgBytes.remaining() <= bufferOut.remaining()) {
        queue.remove();
        bufferOut.put(msgBytes);
      } else {
        return;
      }
    }
  }

  @Override
  public void updateInterestOps() {
    var newKey = 0;
    if (bufferOut.position() > 0) {
      newKey |= SelectionKey.OP_WRITE;
    }
    if (!closed && bufferIn.hasRemaining()) {
      newKey |= SelectionKey.OP_READ;
    }
    if (newKey == 0) {
      silentlyClose();
      return;
    }
    key.interestOps(newKey);
  }

  public void silentlyClose() {
    try {
      fileManager.removeUUIDForFile(uuid);
      sc.close();
    } catch (IOException e) {
      // ignore exception
    }
  }

  @Override
  public void doRead() throws IOException {
    if (sc.read(bufferIn) == -1) {
      closed = true;
    }
    processIn();
    updateInterestOps();
  }

  @Override
  public void doWrite() throws IOException {
    bufferOut.flip();
    sc.write(bufferOut);
    bufferOut.compact();
    processOut();
    updateInterestOps();
  }

  public UUID uuid() {
    return uuid;
  }

  public String pseudo() {
    return pseudo;
  }

  public InetSocketAddress getDownloadAddress() throws IOException {
    final var address = (InetSocketAddress) sc.getRemoteAddress();
    final var hostname = address.getHostString();
    return new InetSocketAddress(hostname, listeningPort);
  }
}

