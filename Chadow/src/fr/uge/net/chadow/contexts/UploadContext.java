package fr.uge.net.chadow.contexts;

import fr.uge.net.chadow.parsing.messages.MD5File;
import fr.uge.net.chadow.parsing.messages.Trame;
import fr.uge.net.chadow.parsing.messages.UploadMessage;
import fr.uge.net.chadow.parsing.readers.IntReader;
import fr.uge.net.chadow.parsing.readers.StringReader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Logger;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;

public final class UploadContext implements Context {

  private enum State {
    WAITING_FNAME, WAITING_FSIZE, PENDING, DONE
  }

  private static final Logger logger = Logger.getLogger(UploadContext.class.getName());
  private final SelectionKey key;
  private final SocketChannel sc;
  private final ByteBuffer bufferIn;
  private final ByteBuffer bufferOut;
  private final StringReader stringReader;
  private final IntReader intReader;
  private final Deque<Trame> queue;
  private boolean closed;
  private State state;
  private FileChannel fileChannel;
  int nbTramesSent = 0;
  private int fileSize;

  public UploadContext(SelectionKey key) {
    this.key = key;
    this.sc = (SocketChannel) key.channel();
    bufferIn = ByteBuffer.allocate(BUFFER_SIZE);
    bufferOut = ByteBuffer.allocate(BUFFER_SIZE);
    stringReader = new StringReader();
    intReader = new IntReader();
    queue = new ArrayDeque<>();
    closed = false;
    state = State.WAITING_FNAME;
  }

  @Override
  public void queueMessage(Trame msg) {
    queue.add(msg);
    processOut();
    updateInterestOps();
  }

  public static boolean readFully(FileChannel fileChannel, ByteBuffer buffer) throws IOException {
    while (buffer.hasRemaining()) {
      if (fileChannel.read(buffer) == -1) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void processIn() throws IOException {
    switch (state) {
      case WAITING_FNAME -> {
        switch (stringReader.process(bufferIn)) {
          case DONE -> {
            fileChannel = FileChannel.open(Path.of(stringReader.get()));
            stringReader.reset();
            state = State.WAITING_FSIZE;
            processIn();
          }
          case ERROR -> silentlyClose();
          case REFILL -> {
          }
        }
      }
      case WAITING_FSIZE -> {
        switch (intReader.process(bufferIn)) {
          case DONE -> {
            fileSize = intReader.get();
            intReader.reset();
            state = State.PENDING;
            processIn();
          }
          case ERROR -> silentlyClose();
          case REFILL -> {
          }
        }
      }
      case PENDING -> {
        bufferIn.flip();
        while (bufferIn.remaining() >= Integer.BYTES) {
          final var offSet = BUFFER_SIZE - Integer.BYTES;
          int min = clamp(fileSize - (offSet * nbTramesSent) - Integer.BYTES, 0, offSet);

          if (min == 0) {
            state = State.DONE;
            break;
          }
          min = (min < offSet) ? min + Integer.BYTES : offSet;
          final var tmpBuffer = ByteBuffer.allocate(min);
          var nbTrame = bufferIn.getInt();
          final var position = nbTrame * offSet;
          fileChannel.position(position);
          if (!readFully(fileChannel, tmpBuffer)) {
            logger.warning("Client closed connection");
            return;
          }
          queueMessage(new UploadMessage(tmpBuffer.position(), tmpBuffer));
        }
        bufferIn.compact();
      }
    }
  }

  private static int clamp(int value, int min, int max) {
    return Math.min(Math.max(value, min), max);
  }


  @Override
  public void processOut() {
    if (state != State.PENDING) {
      logger.severe("Should not be here !");
      silentlyClose();
    }
    while (!queue.isEmpty()) {
      var msg = queue.peek();
      var msgBytes = msg.toByteBuffer().flip();
      if (msgBytes.remaining() <= bufferOut.remaining()) {
        queue.remove();
        bufferOut.put(msgBytes);
        nbTramesSent++;
      } else {
        return;
      }
    }
  }

  @Override
  public void doWrite() throws IOException {
    bufferOut.flip();
    sc.write(bufferOut);
    bufferOut.compact();
    processOut();
    updateInterestOps();
  }

  public void doRead() throws IOException {
    if (sc.read(bufferIn) == -1) {
      closed = true;
    }
    processIn();
    updateInterestOps();
  }


  @Override
  public void updateInterestOps() {

    var newKey = 0;
    if (bufferOut.position() > 0) {
      newKey |= SelectionKey.OP_WRITE;
    }
    if (!closed && bufferIn.hasRemaining()) {
      newKey |= SelectionKey.OP_READ;
    }
    if (newKey == 0) {
      silentlyClose();
      return;
    }
    key.interestOps(newKey);
  }

  public void silentlyClose() {
    logger.warning("Closing...");
    try {
      if (fileChannel != null) {
        fileChannel.close();
      }
      sc.close();
    } catch (IOException e) {
      // ignore exception
    }
  }

  @Override
  public void doConnect() throws IOException {
    if (sc.finishConnect()) {
      key.interestOps(SelectionKey.OP_READ);
    }
  }
}
