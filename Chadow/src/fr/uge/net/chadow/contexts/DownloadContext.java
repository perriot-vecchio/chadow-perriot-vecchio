package fr.uge.net.chadow.contexts;

import fr.uge.net.chadow.client.DownloadTracker;
import fr.uge.net.chadow.parsing.messages.Trame;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Logger;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;
import static fr.uge.net.chadow.Configuration.UTF_8;

public final class DownloadContext implements Context {
  private static final Logger logger = Logger.getLogger(DownloadContext.class.getName());
  private final SelectionKey key;
  private final SocketChannel sc;
  private final ByteBuffer bufferIn;
  private final ByteBuffer bufferOut;
  private final Deque<Trame> queue;
  private final DownloadTracker tracker;
  private final String fileName;
  private final int fileSize;
  private final FileChannel fileChannel;
  private boolean closed;
  private int index;

  public DownloadContext(SelectionKey key, DownloadTracker tracker, String fileName, int fileSize, FileChannel fileChannel) throws IOException {
    this.key = key;
    this.tracker = tracker;
    this.fileName = fileName;
    this.fileSize = fileSize;
    this.fileChannel = fileChannel;
    sc = (SocketChannel) key.channel();
    bufferIn = ByteBuffer.allocate(BUFFER_SIZE);
    bufferOut = ByteBuffer.allocate(BUFFER_SIZE);
    queue = new ArrayDeque<>();
    closed = false;
    init();
  }

  private void init() {
    queueMessage(() -> {
      final var encodedFileName = UTF_8.encode(fileName);
      final var buffer = ByteBuffer.allocate(BUFFER_SIZE);
      buffer.putInt(encodedFileName.remaining()).put(encodedFileName).putInt(fileSize);
      return buffer;
    });
    index = tracker.next(this);
    queueMessage(() -> ByteBuffer.allocate(Integer.BYTES).putInt(index));
  }

  public void queueMessage(Trame msg) {
    queue.add(msg);
    processOut();
    updateInterestOps();
  }

  @Override
  public void processIn() throws IOException {
    bufferIn.flip();
    while (bufferIn.hasRemaining()) {
      final var size = bufferIn.getInt();
      var position = Long.min(fileSize - size, (long) index * (BUFFER_SIZE - Integer.BYTES));
      fileChannel.position(position).write(bufferIn);
    }
    bufferIn.compact();
    index = tracker.next(this);
    queueMessage(() -> ByteBuffer.allocate(Integer.BYTES).putInt(index));
  }

  @Override
  public void processOut() {
    while (!queue.isEmpty()) {
      var msg = queue.peek();
      var msgBytes = msg.toByteBuffer().flip();
      if (msgBytes.remaining() > bufferOut.remaining()) {
        return;
      }
      queue.remove();
      bufferOut.put(msgBytes);
    }
  }

  @Override
  public void doWrite() throws IOException {
    bufferOut.flip();
    sc.write(bufferOut);
    bufferOut.compact();
    processOut();
    updateInterestOps();
  }

  public void doRead() throws IOException {
    if (sc.read(bufferIn) == -1) {
      closed = true;
    }
    processIn();
    updateInterestOps();
  }

  @Override
  public void updateInterestOps() {
    var newKey = 0;
    if (bufferOut.position() > 0) {
      newKey |= SelectionKey.OP_WRITE;
    }
    if (!closed && bufferIn.hasRemaining()) {
      newKey |= SelectionKey.OP_READ;
    }
    if (newKey == 0) {
      silentlyClose();
      return;
    }
    key.interestOps(newKey);
  }

  @Override
  public void doConnect() throws IOException {
    if (!sc.finishConnect()) {
      logger.warning("the selector gave a bad hint !");
      return;
    }
    key.interestOps(SelectionKey.OP_WRITE);
  }

  public void silentlyClose() {
    try {
      sc.close();
    } catch (IOException e) {
      // ignore exception
    }
  }
}
