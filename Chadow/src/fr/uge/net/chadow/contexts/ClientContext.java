package fr.uge.net.chadow.contexts;

import fr.uge.net.chadow.client.DownloadTracker;
import fr.uge.net.chadow.parsing.messages.*;
import fr.uge.net.chadow.parsing.readers.Reader;
import fr.uge.net.chadow.parsing.readers.TrameReader;
import fr.uge.net.chadow.parsing.types.ServerClientType;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Logger;

import static fr.uge.net.chadow.Configuration.BUFFER_SIZE;


public final class ClientContext implements Context {
  private static final Logger logger = Logger.getLogger(ClientContext.class.getName());
  private final Selector selector;
  private final SelectionKey key;
  private final SocketChannel sc;
  private final ByteBuffer bufferIn;
  private final ByteBuffer bufferOut;
  private final Deque<Trame> queue;
  private final TrameReader messageReader;
  private final DownloadTracker downloadTracker;
  private boolean closed;

  public ClientContext(Selector selector, SelectionKey key, int listenerPort, String login) {
    this.selector = selector;
    this.key = key;
    this.sc = (SocketChannel) key.channel();
    bufferIn = ByteBuffer.allocate(BUFFER_SIZE);
    bufferOut = ByteBuffer.allocate(BUFFER_SIZE);
    queue = new ArrayDeque<>();
    messageReader = new TrameReader(ServerClientType.class);
    downloadTracker = new DownloadTracker();
    closed = false;

    init(new Authentication(listenerPort, login));
  }

  private void init(Authentication auth) {
    System.out.println("CHADOW\nHello " + auth.login() + " !");
    queue.add(auth);
    processOut();
  }

  @Override
  public void processIn() throws IOException {
    for (; ; ) {
      Reader.ProcessStatus status = messageReader.process(bufferIn);
      switch (status) {
        case DONE:
          final var msg = messageReader.get();
          switch (msg) {
            case PublicMessage _, PrivateMessage _, FileListMessage _, SharingMessage _ -> System.out.println(msg);
            case TransactionMessage transactionMessage -> {
              final var fileName = transactionMessage.fileName();
              final var fileChannel = FileChannel.open(Path.of(fileName),
                      StandardOpenOption.CREATE, StandardOpenOption.WRITE);
              transactionMessage.forEachProvider(provider ->
                      launchDownload(provider, fileName, transactionMessage.fileSize(), fileChannel));
            }
            default -> throw new IllegalStateException("Unexpected value: " + msg);
          }
          messageReader.reset();
          break;
        case REFILL:
          return;
        case ERROR:
          silentlyClose();
          return;
      }
    }
  }

  private void launchDownload(InetSocketAddress provider, String fileName, int fileSize, FileChannel fileChannel) {
    try {
      final var downloadSc = SocketChannel.open();

      downloadSc.configureBlocking(false);
      downloadSc.connect(provider);

      final var downloadKey = downloadSc.register(selector, SelectionKey.OP_CONNECT);
      downloadKey.attach(new DownloadContext(downloadKey, downloadTracker, fileName, fileSize, fileChannel));
      downloadKey.interestOps(SelectionKey.OP_CONNECT);
    } catch (IOException ioe) {
      throw new UncheckedIOException(ioe);
    }
  }

  @Override
  public void queueMessage(Trame msg) {
    queue.add(msg);
    processOut();
    updateInterestOps();
  }

  @Override
  public void processOut() {
    while (!queue.isEmpty()) {
      var msg = queue.peek();
      var msgBytes = msg.toByteBuffer().flip();
      if (msgBytes.remaining() > bufferOut.remaining()) {
        return;
      }
      queue.remove();
      bufferOut.put(msgBytes);
    }
  }

  @Override
  public void updateInterestOps() {
    var newKey = 0;
    if (bufferOut.position() > 0) {
      newKey |= SelectionKey.OP_WRITE;
    }
    if (!closed && bufferIn.hasRemaining()) {
      newKey |= SelectionKey.OP_READ;
    }
    if (newKey == 0) {
      silentlyClose();
      return;
    }
    key.interestOps(newKey);
  }

  public void silentlyClose() {
    try {
      sc.close();
    } catch (IOException e) {
      // ignore exception
    }
  }

  @Override
  public void doRead() throws IOException {
    if (sc.read(bufferIn) == -1) {
      closed = true;
    }
    processIn();
    updateInterestOps();
  }

  @Override
  public void doWrite() throws IOException {
    bufferOut.flip();
    sc.write(bufferOut);
    bufferOut.compact();
    processOut();
    updateInterestOps();
  }

  @Override
  public void doConnect() throws IOException {
    if (!sc.finishConnect()) {
      logger.warning("the selector gave a bad hint !");
      return;
    }
    key.interestOps(SelectionKey.OP_WRITE);
  }
}
