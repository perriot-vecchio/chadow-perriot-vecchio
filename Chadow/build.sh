#!/bin/bash

# Répertoire du projet
PROJECT_DIR="."

# Nom du JAR
JAR_SERVER="ServerChadow.jar"
JAR_CLIENT="ClientChadow.jar"

# Chemin de la classe principale
MAIN_SERVER="fr.uge.net.chadow.server.Server"
MAIN_CLIENT="fr.uge.net.chadow.client.Client"

# Supprimer le fichier JAR existant
if [ -f "$PROJECT_DIR/$JAR_SERVER" ]; then
    rm "$PROJECT_DIR/$JAR_SERVER"
fi
if [ -f "$PROJECT_DIR/$JAR_CLIENT" ]; then
    rm "$PROJECT_DIR/$JAR_CLIENT"
fi

# Compilation du projet
javac -d "$PROJECT_DIR/bin" "$PROJECT_DIR/src/fr/uge/net/chadow/*.java"

# Création du fichier Manifest server
echo "Manifest-Version: 1.0" > "$PROJECT_DIR/Manifest.mf"
echo "Main-Class: $MAIN_SERVER" >> "$PROJECT_DIR/Manifest.mf"

# Générer le fichier JAR server
jar cfm "$PROJECT_DIR/$JAR_SERVER" "$PROJECT_DIR/Manifest.mf" -C "$PROJECT_DIR/bin" .

# Supprimer le fichier Manifest server
rm "$PROJECT_DIR/Manifest.mf"


# Création du fichier Manifest client
echo "Manifest-Version: 1.0" > "$PROJECT_DIR/Manifest.mf"
echo "Main-Class: $MAIN_CLIENT" >> "$PROJECT_DIR/Manifest.mf"

# Générer le fichier JAR client
jar cfm "$PROJECT_DIR/$JAR_CLIENT" "$PROJECT_DIR/Manifest.mf" -C "$PROJECT_DIR/bin" .

# Supprimer le fichier Manifest client
rm "$PROJECT_DIR/Manifest.mf"

echo "Build terminé. Fichier JAR générés : $PROJECT_DIR/$JAR_SERVER $PROJECT_DIR/$JAR_CLIENT"