
<div style="text-align:left;">
PERRIOT Mathieu</br>
VECCHIO Matias</br>
</div>

# Chadow -- Protocole

## Etat du document

Ce document apporte des informations sur le protocole Chadow. nous attendons des retours et des suggestions pour des améliorations. Référez vous à cette version comme standard si aucune autre version n'a été publiée plus récemment.
La distribution/modification de ce document est totalement libre.

## Résumé

Le but du projet Chadow est de réaliser un protocole au dessus de TCP pour un serveur de discussion en ligne (à la IRC ou Discord) qui permet aux utilisateurs connectés de s'échanger des fichiers sans passer par le serveur. Un utilisateur connecté au serveur pourra télécharger un fichier directement chez un ou plusieurs utilisateurs possédant ce fichier. Le protocole prend en charge deux méthodes de téléchargement : le mode ouvert et le mode caché.
Le mode ouvert permet aux utilisateur de télécharger des fichiers de façon visible.
Le mode caché permet aux utilisateurs de cacher leur adresse IP des personnes chez qui ils téléchargent les fichiers.

## Terminologie :

- "int" -> "Int" -> Integer (4 octets)
- "long" -> Long (8 octets)
- "BE" -> Big Endian -> les bits de points forts sont placé au début.
- "MD5" -> représente un code en 16 octets provenant d'un encodage avec MD5
- "UTF-8" -> Une chaine de caractères de taille variable encodée en UTF-8.


## Table des matières

- [Echange Client - Serveur](##Echange-Client---Serveur)
   - [Message automatique au lancement](#message-automatique-au-lancement)
   - [Envoie de message transmis à tous les clients connectés](#envoie-de-message-transmis-à-tous-les-clients-connectés)
   - [Envoie de message transmis à un client spécifique identifié par son pseudonyme](#envoie-de-message-transmis-à-un-client-spécifique-identifié-par-son-pseudonyme)
   - [Annonce d'un ou des fichiers disponible pour le téléchargement](#annonce-dun-ou-des-fichiers-disponible-pour-le-téléchargement)
   - [Annonce qu'un ou plusieurs fichiers ne sont plus disponible pour le téléchargement](#annonce-quun-ou-plusieurs-fichiers-ne-sont-plus-disponible-pour-le-téléchargement)
   - [Demande au serveur de la liste des fichiers disponibles au téléchargement](#demande-au-serveur-de-la-liste-des-fichiers-disponibles-au-téléchargement)
   - [Demande de téléchargement d'un fichier en mode caché ou ouvert](#demande-de-téléchargement-dun-fichier-en-mode-caché-ou-ouvert)
- [Echange Client - Client](#echange-client---client)

## Echange Client - Serveur : 

7 types d'envoie de message au serveur :

- envoyer des messages qui seront transmis à tous les clients connectés. (ALL -> 0)
- envoyer des messages qui seront destinés à un seul client identifié par son pseudonyme.(PRIVATE -> 1)
- le client peut annoncer au serveur qu'il propose un ou des fichiers pour le téléchargement par les autres clients (PUBLISH -> 2)
- le client peut annoncer au serveur qu'il ne propose plus un ou des fichiers pour le téléchargement par les autres clients (REMOVE -> 3)
- le client peut demander au serveur la liste des fichiers disponibles au téléchargement (LIST -> 4)
- un client peut demander à télécharger un fichier soit en mode ouvert, soit en mode caché (comme détaillé ci-après). (ASKING -> 5)
- un client vient de se connecter. CONNEXION -> 6
- un client déclare son port d'écoute pour proxy -> 7
- un client répond à la demande d'un fichier -> 9

</br>

6 types de réponse au client :
- MESSAGE -> 0
- RENAME -> 1
- FILES_LIST -> 2
- SHARING -> 3
- ABORT -> 4
- TRANSACTION -> 5
- GROUP -> 6
- PROXY -> 7


### Message automatique au lancement.
- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour le port d'écoute du client.
- Un int (Big Endian) pour la taille du pseudo une fois encodé en UTF-8.
- Le pseudo encodé en UTF-8.

```
****************************************************
*   id         *  port     * taille    *  pseudo   *
*  (CONNEXION) *  d'écoute * pseudo    *     en    *
* int (BE)     *  int (BE) * int (BE) *   UTF-8   *
****************************************************
```


### Envoie de message transmis à tous les clients connectés
- Un int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour la taille du message une fois encodé en UTF-8.
- Le message encodé en UTF-8.

```
*************************************************************
*   id       *  taille  *           * taille    *  message  *
*  (ALL)     *  pseudo  *  Pseudo   * message   *           *
* int (BE)   *   (BE)   *    UTF-8  * int (BE) *   UTF-8   *
*************************************************************
```

#### Reponse du serveur :
-> le serveur s'occupera d'envoyer le messages à tous utilisateurs connectés.

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour la taille du pseudonyme encodé en UTF-8.
- Le pseudonyme encodé en UTF-8.
- Un Int (Big Endian) pour la taille du message encodé en UTF-8.
- Le message encodé en UTF-8.

```
*************************************************************
*   id      * taille    * pseudo    * taille    *  message  *
* (MESSAGE) * pseudo    *	         * message   *           *
* int (BE)  * int  (BE) *  UTF-8    * int  (BE) *  UTF-8    *
*************************************************************
```

### Envoie de message transmis à un client spécifique identifié par son pseudonyme

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour la taille du pseudonyme encodé en UTF-8.
- Le pseudonyme encodé en UTF-8.
- Un Int (Big Endian) pour la taille du message encodé en UTF-8.
- Le message encodé en UTF-8.

```
*****************************************************************
*   id      * taille    * pseudo        * taille    *  message  *
* (PRIVATE) * pseudo    * destinataire  * message   *           *
* int (BE)  * int (BE)  *  UTF-8         * int (BE) *  UTF-8    *
*****************************************************************
```

#### Reponse du serveur :

-> le serveur s'occupera d'envoyer le messages à l'utilisateur "pseudo" concerné.

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour la taille du pseudonyme encodé en UTF-8.
- Le pseudonyme encodé en UTF-8.
- Un Int (Big Endian) pour la taille du message encodé en UTF-8.
- Le message encodé en UTF-8.

```
*************************************************************
*   id      * taille    * pseudo    * taille    *  message  *
* (MESSAGE) * pseudo    * du sender * message   *           *
* int (BE)  * int (BE) *  UTF-8    * int (BE) *  UTF-8    *
*************************************************************
```

### Annonce d'un ou des fichiers disponible pour le téléchargement

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour le nombre de fichiers disponible pour le téléchargement

```
*****************************
*   id      * nb fichier    *
* (PUBLISH) *               *
* int (BE)  * int (BE)     *
*****************************
```

Puis, par fichiers :
- Un Int (Big Endian) pour la taille du nom du fichier encodé en UTF-8.
- Le nom du fichier encodé en UTF-8.
- 16 octets pour la valeur du fichier encodé avec MD5.
```
******************************************************
*   taille nom  *  nom      * valeur    * taille du  *
*   fichier     *  fichier  * fichier   *  fichier   *
*   int (BE)   *  UTF-8    * MD5        *   int (BE) *
******************************************************
```

#### Reponse du serveur :
-> Le serveur indique si tout est bon ou si le fichier a été renommé car il y a un conflit de nom.

- Un Int (Bid Endian) comme id pour transmettre le type de message.
- Un Byte pour indiqué si le fichier à été renommé par le serveur.
- Un Int (Big Endian) pour la taille du nom du fichier encodé en UTF-8.
- Le nom du fichier renommé encodé en UTF-8

``` 
*********************************************************
*   id      *   rename      *   taille du   *  nouveau  *
* (RENAME)  *   ou non      *   nouveau nom *   nom     *
* int (BE)  * byte (0 ou 1) *     int      *   UTF-8   *
*********************************************************
```

### Annonce qu'un ou plusieurs fichiers ne sont plus disponible pour le téléchargement

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour le nombre de fichiers disponible pour le téléchargement

```
*****************************
*   id      * nb fichier    *
* (REMOVE)  *               *
* int (BE)  * int (BE)     *
*****************************
```

Puis, par fichiers (concaténé à la trame ci-dessus):
- Un Int (Big Endian) pour la taille du nom du fichier encodé en UTF-8.
- Le nom du fichier encodé en UTF-8.

```
*****************************
*   taille nom  *  nom      *
*   fichier     *  fichier  *
*   int (BE)   *  UTF-8    *
*****************************
```

</br>
</br>
</br>

### Demande au serveur de la liste des fichiers disponibles au téléchargement

- Un Int (Big Endian) comme id pour transmettre le type de message.

```
*************
*   id      * 
*  (LIST)   *
* int (BE)  *
*************
```

#### Reponse du serveur :
-> Le serveur renvoie la liste des enregistrements

- Un Int (Big Endian) pour le nombre de fichiers disponible pour le téléchargement
    
```
*************************
*   id      * nombre    *
* (LIST)    * fichier   *
* int (BE)  * int (BE) *
*************************
```

Puis, par fichiers (concaténé à la trame ci-dessus) :
- Un Int pour la taille du nom du fichier encodé en UTF-8.
- Le nom du fichier encodé en UTF-8.

```
*****************************
*   taille nom  *  nom      *
*   fichier     *  fichier  *
*   int (BE)   *  UTF-8    *
*****************************
```

### Demande de téléchargement d'un fichier en mode caché ou ouvert.

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Octet valant 1 ou 0 pour définir si l'utilisateur veut être caché ou non.
- Un Int (Big Endian) pour la taille du nom du fichier encodé en UTF-8.
- Le nom du fichier encodé en UTF-8.

```
******************************************************
*    id      * caché     * taille nom    * nom       *
* (ASKING)   * ou ouvert * fichier       *  fichier  * 
*  int (BE)  * 1 ou 0    * int (BE)      * UTF-8     *
******************************************************
```

-> Le serveur va demandé à chaques client avec le fichier s'ils veulent le partager.

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) comme id de transaction
- Un Int (Big Endian) pour la taille du nom du fichier encodé en UTF-8.
- Le nom du fichier encodé en UTF-8.
- Un Octet valant 1 ou 0 pour définir si l'utilisateur veut être caché ou non.

```
*************************************************************************
*    id      *  transaction  *   taille nom    * nom       * caché      *
* (SHARING)  *    id         *   fichier       * fichier   * ou ouvert  * 
*  int (BE)  *   int (BE)    *    int (BE)     * UTF-8      * 1 ou 0    *
*************************************************************************
```

**Si ouvert** (concaténé à la trame ci-dessus):
- Un Long (Big Endian) pour la taille du pseudonyme encodé en UTF-8.
- Le pseudonyme encodé en UTF-8.

```
*************************
* taille    * pseudo    *
* pseudo    *           *
* int (BE) * UTF-8      *
*************************
```

-> Réponse du client avec un octet, 0 s'il ne veut pas partagé son fichier, 1 sinon.
```
********************************************************
*         id            *              *      id       *
*  Pending Transaction  *   0 ou 1     *  transaction  *
*      int (BE)         *    Byte      *   int (BE)     *
********************************************************
```

On acquite dans tous les cas la bonne réception du message par la trame suivante :

```
***************************************************************************************
*      id       *       Size      *     Name       *   taille du      *   nbPersonne  *
* (TRANSACTION) *     Name File   *     File       * fichier en octet *               *
*  int (BE)     *      int (BE)   *     UTF8       *     int (BE)     *   int (BE)    *
***************************************************************************************
```
</br>
</br>

si la transaction est possible (il y a au moins une personne) on concatène à la trame ci-dessus: 
```
*******************************************
*     taille      *           *           *
*     adresse     *  adresse  *  port     *
*     int (BE)    *  UTF-8    *  int (BE) *
*******************************************
```


L'addresse IP renseigné peut alors correspondre soit au destinataire finale du fichier, soit à un proxy.


### Ordre de proxy d'un client.

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Long (Big Endian) pour la taille de l'addresse IP du destinataire du fichier encodée en UTF-8.
- L'addresse IP du destinataire du fichier encodé en UTF-8.

```
*******************************************
*    id      * taille de      * Addresse  *
* (PROXY)    * l'addresse IP  *   IP      *
*  int (BE)  * long (BE)      *  UTF-8    *
*******************************************
```

Le client va alors répondre avec son port d'écoute :

- Un Int (Big Endian) comme id pour transmettre le type de message.
- Un Int (Big Endian) pour le port d'écoute du proxy.

```
*******************************
*    id      *    port        *
* (PROXY)    *    d'écoute    *
*  int (BE)  *    int (BE)    *
*******************************
```

## Echange Client - Client :

Le client qui a besoin du fichier envoie le numéro de tronçon dont il a besoin au(x) client(s) concerné par la transaction.

```
*************
*           *
*  numéro   *
*  int (BE) *
*************
```

Le client qui envoie les données envoie la trame suivante : 

```
*************************
*           *           *
*  nbOctet  *  Données  *
*  int(BE)  *  Byte     *
*************************
```
